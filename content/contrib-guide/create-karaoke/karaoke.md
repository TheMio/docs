+++
title = "Making the karaoke"
weight = 2
+++

You now have your video and lyrics, it's time to start. This page will guide you into making your first karaoke video. Timing is syncing lyrics to the audio data of your karaoke.

## Subtitle styles

You first need a *style* for your karaoke subtitles. [Click on this link](/aegisub/sampleKaraokeMugen.ass) to download the example file. Right click and save it anywhere you can find it easily. **You will use it each time you want to synchronize some lyrics**.

## Workspace

Open the example file with **Aegisub**. You first have an almost empty user interface.

![ass](/images/creation/AegisubSample.png)

- **You would like to synchronise some subtitles**

Go to **"Video > Open a video"**, and select your video. You can also "drag & drop" directly onto the interface.

- **You would like to time a simple audio file because you did not find a satisfying video or with a good enough quality (an mp3 for example)**

Use **"Audio > Open an audio file"**, and select your audio file. You can also "drag & drop" it directly on the user interface.

Go then to the **Video > Use a fake video** menu, pick the "HD 1080p" quality (unless you only have an album cover in "classic" 4/3 format at your disposal). The background color is up to your choice, along with the video duration (put more pictures than the total duration of the audio file, it's not important) and proceed.

![ass](/images/creation/dummyvideoaudio.png)

- After having integrated you file, you should have a screen similar to this one, where we're making a synchronization for the *Ashita no Nadja* anime. On this screen, if you synchronize with an audio file, the only difference is that you'll get a screen filled with the color you chose.

![ass](/images/creation/Ecran01bis.png)

**There are two differences with audio-only :**

1. If you don't have an audio track at the top right of the screen and if you get no sound when playing your video via the ![ass](/images/creation/IcoLecture.PNG) button, it means sound was not loaded. Fix this with the **Audio > Open the video audio** menu.

2. If you do not have the same audio track, that's nomal! Fix it by clicking the ![ass](/images/creation/IcoSpectre.PNG) icon. This kind of view can be easier to understand.

- Once the video is set **(or the audio is)**  go to the **File** > **Properties** menu and put a resolution of 0 x 0, so you won't have to worry about having too-small font on screen. This must be set ***every time*** you open a sample file, open a video / an mp3, or open time-synchronised subtitles with Aegisub. If you open an .ass file you wish to modify and forget doing this you will see that your lines became too small compared to the previous time you tried your karaoke.

- Also tick **"Shadow and edges scale"**, it allows to apply the same style on Karaoke Mugen **and** on [Live](https://live.karaokes.moe).

Example picture:

![ass](/images/creation/ResolutionSample5.png)
![ass](/images/creation/ResolutionSample6.png)

## "Simple" synchronization (or *timing*)

For this step, we advise you to disable **automatic replacement of the video cursor at the time of the start of the current line** by clicking on the ![ass](/images/creation/IcoAuto.PNG) button in order to make your work easier.

The goal for this step is to synchronize every lyrics *line* to the screen. We will then synchronise syllable by syllable later.

**Look carefully at those three elements of "subtitle box" :**

![ass](/images/creation/BoxSousTitres.PNG)

*(1) : your subtitles' appearing time.*

*(2) : your subtitles' disappearing time.*

The aim of this step is to define the appearance and disapearance time of the subtitle.

Here's what you'll have to do :

1. Read your video with the ![ass](/images/creation/IcoLecture.PNG) button under its preview and pause with![ass](/images/creation/IcoPause.PNG). To the right of the ![ass](/images/creation/IcoAuto.PNG) button you'll find the time you've paused at with the *hour:minute:second:secondhundreth* format.
2. While on the right line, keep pressing `CTRL + 3` so that time becomes the start of your line.
3. Resume playing the video and pause when your current line's singing stops.
4. While on the right line, keep pressing `CTRL + 4` so that time becomes the end of your line.
5. Add text in the freshly new made line.
6. Right click under your line and then **insert** to create a new one.
7. Select this new line and repeat the process until all your lines are synchronized.

{{% notice tip "I'm Sonic The Hedgehog!" %}}
If you go too fast while playing your audio/video, the ![ass](/images/creation/IcoLectureLigne.PNG) button allows you to resume playing from the start of a line. Useful if you struggle a bit with the cursor under the video preview ! You can also use the keyboard arrows left and right to go foward or backward frame by frame in your video.**
{{% /notice %}}

## Syllable synchronization (or *k time*)

Once all the lines are synced to the video. It is now time to synchronize all your lines syllable by syllable.

Before you begin, it's recommended to save your work in a new file ("Save subtitles as").

The main idea is to first display your subtitle line in white then fill it with an orange color as your song goes. For this we will use **tags**, and more precisely the `{\k}` tag.

Every syllable in our subtitle line will be separated by a {\kX} tag, where "X" is the hundreth of second before your text after the tag turns white, until the next tag.

As an example, the first line will turn from `akaku somatta rashinban` to `{\k11}a{\k26}ka{\k19}ku {\k24}so{\k24}mat{\k23}ta {\k11}ra{\k23}shi{\k28}n{\k23}ba{\k13}n`.

Fortunately you don't have to precisely measure every hundreth of second of each syllable: Aegisub has a **Karaoke Assistant**! You can enable it by clicking the  ![ass](/images/creation/IcoKaraoke.PNG) button (last icon of the audio sprectre..

You will notice two changes on the upper right part of your user interface: your text now appears word per word on your sound sprectre, and that same text appears pre-cut under the sprectre. It is also now possible to make an horizontal and/or vertical zoom so don't hesitate to overuse it.

![ass](/images/creation/Ecran02.PNG)

To *k-time* with the karaoke assistant, you'll first need to outline your syllables. Let's focus on our copied line, just under the audio spectrum. If you hover it with your mouse, you'll see a cursor on that box This is the part of the screen where you'll outline syllables, by clicking at every needed intersection.

Here are some rules for that:

![ass](/images/creation/TableKana.png)

- For japanese karaokes, a "syllable" is a kana (see the table above).

- For double letters *(ex. : somatta)*, you have two solutions depending on what you can hear: either you cut **so | ma | t | ta**  if you consider that the "*a*" is said twice. The "*t*" alone will be there to mark the second time the "*a*" will be sung. On the contrary, if the  "*a*" is said once you cut as **"so | mat | ta"** (and not **"so | ma | tta"**).

- In any case, you must never ever have a double sound said on one syllable cutting. For example, if it's **"so | ma | t | ta"** but you cut as **"so | mat | ta"**, you'll have a small blank during which the singer will double the **"a"** sound and your sync will stay on **mat**. You must absolutely avoid that kind of error.

- Some syllable might be hard to distinguish from each other. Don't hesitate to gather both in one. Example: according to the hiragana/katakana table above, **"shin"** should be cut as **"shi | n"** but if it's said as **"shin"**, that's only one block.

Here's an example :

![ass](/images/creation/ExTimeK.PNG)

- Remember that you don't have to cut a syllable if the singer doesn't either. It's especially true for japanese, but also for other languages like french.

- Once you're satisfied with how you cut the sentence, you can click on ![ass](/images/creation/IcoTimeOK.PNG). If you did a mistake, you can delete it by clicking on it again. If you want to delete the whole line you can also do it with [ass](/images/creation/IcoTimeNOK.PNG).

- Once you confirmed everything, you'll see a few changes on the user interface: the syllable cutting is applied to the spectrum, just where you did it before, and **{\k}** tags were generated with more or less random times.

![ass](/images/creation/Ecran03.PNG)

Then all you have to do is fiddle witht he yellow dot lines on the spectrum to match each zone on it to a sung syllable! Click and hold the cursor on any of those and move it exactly where it's suppoed to be sung. to help yourself, you can use:

- Your ear
- The ![ass](/images/creation/IcoLecture.PNG), ![ass](/images/creation/IcoPause.PNG) and ![ass](/images/creation/IcoLectureLigne.PNG) buttons which you knwo already by heart (don't hesitate to overuse the third button and to listen several times at the lines until you are astisfied with the synchronization.)
- The audio spectrum

{{% notice note "The white bar" %}}
A white bar is displayed while the video is played, but unfortunately it diseappears when paused.
{{% /notice %}}

{{% notice tip "How to listen to a particular syllable" %}}
Do not hesitate to right click **between** every yellow line as this'll allow you to only listen to one particular syllable, and it's **very handy**. You can also use the right and left arrows on your keyboard while you're on the spectrum in order to listen again to every syllable one by one, and adjust the yellow lines with accuracy.
{{% /notice %}}

Having the bad habit of cutting before the sentence is highly discouraged. Your line will appear and have an empty spot right before the first syllable is sang. To help you with starting your line exactly when the first syllable starts without any delay, you can do as follows:

- Make the sentence start in advance
- Place your first line with yellow points at the right spot
- Note the time you'll have placed the line with yellow points at
- Use that new time as if it was the real starting point of the sentence
- Remove the line with yellow points.

With that your sentence won't have any pre-singing blank.

![ass](/images/creation/doublebalisek.png)

That step is long and tiresome, you might not be in rythm on your first attemps but don't give up: speed and precision comes with training.

{{% notice tip "How to deal with fast videos" %}}
If your video moves too fast and you can't focus on the right timing of syllable you can try to synchronize with a fake video. For this, go to the **Video > Use a fake video** menu. Take note of your original video's resolution (or use a preset), the fps (number of frames per second) along with its duration and choose a background color of your choice. Remember to set the resolution to 0 x 0 in **Properties** and voilà, you can now continue your timing on a unified color background and keep the original audio track. Don't worry: when you'll save your work the right video will be used in Karaoke Mugen.

![ass](/images/creation/FausseVideo.png)

{{% /notice %}}

{{% notice tip "Synchronization in theory and in practice" %}}
During synchronization, you can try to have each syllable start the moent where the first vowel is sung (instead of the first syllable). It's more easier and natural for users to see a syllable filling at this very moment.
	- For example, if at some time the singer says, *je ne sais pas* or *My dear mother* and that he spends a lot of time on the "M" of "mother", it's not uncommon to make the "mo" syllable (from mother) start at the very moment he sings the "o" (and not when the "m" is sung).
	- You can apply this to almost any word, especially to the those starting with "ch / sh", "y", "pr / cr / gr / tr / etc".
{{% /notice %}}

### Progressive filling

The latin languages having less sharp cutting by syllables, it's possible that you'll prefer a "progressive" filling of the orange color on the white colour, like with some popular karaokes that you can find on the internet. The **{\kf}** tag is here for this.

Once your karaoke is synchronized correctly (with \k tags), you can modify the type of filling and turn it into "kf / progressive". To do this, go to **Edit > Search and replace**, and search `{\k` (yes... without closing the bracket) to replace it by `{\kf` (once more, do not close the bracket). Click on **Replace everything** and now your karaoke has a full progressive syllable filling, every \k tag having been turned into \kf tags !

Also, nothing prevents you from using {\k} and {\kf} tags at the same time. For example on some japanese songs, singers may end some of their sentences on a long syllable. It can be interesing to put a {\kf} there. However, beware of using them too much, it's usually considered usuless to put a progressive tag on a long syllable that lasts less than 1 second.

{{% notice info "How progressive filling works" %}}
By default, progressive filling of a syllable ends when the next tag is played (thus the following syllable). However, a progressive syllable can sometimes is finished singing way before the next syllable.

To fix this, don't hesitate to add another syllable cut *right after* your kf tag but *before* the next syllable. You can thus control precisely at which moment the syllable will be completely filled. This can be worth it on some songs.

![ass](/images/creation/doublekf.png)

*Here, the singer makes a pause after "terre de"*

![ass](/images/creation/kfsyllabe.png)

*Here, the sentence will be completely filled just before disappearing from the screen*

{{% /notice %}}

## Style change depending on the karaoke type (optional)

As you might have noticed, the sample file has a few styles :

- Sample KM [Up]
- Sample KM [Down]
- Sample KM Duo [Voice 1]
- Sample KM Duo [Voice 2]
- Sample KM Duo [Voice 1&2]
- Sample KM [Choir]

**Up** is the usual line style that you'll use most often.

If you make a karaoke that can be sung [in duo](https://live.karaokes.moe/?video=057ff76e-ca7b-42ba-a7cc-34b6a75315fe), the styles with `Duo` in them are a better fit.

If your karaoke has background voices, you can use the `Choir` style. It appears ([between the middle and the top of the screen](https://live.karaokes.moe/?video=8f42fecb-c15a-47d8-98da-9b7350a7f132)), or with  the `Down` style ([at the bottom of the screen](https://live.karaokes.moe/?video=fcd455f9-839d-403f-a971-88b8481ae8b0)).

If you wish to change the style of several lines, all you have to is to select them and use the corresponding style.

![ass](/images/creation/ChangementStyle.png)

## Applying the script

**A fundamental Karaoke Mugen rule is that subtitles must be readable. So, a line of text should appear approximatively one second before it has to be sung.**

Once the "k" synchronization has been completed, a script must be applied. It will allow to make each line appear approximatively 1 second before and add a fade effect.

The script is already included in the sample file that you used. It's the first line that you see on top. All you needed is apply it going to the **Automatism** > **Apply the karaoke template** menu

{{% expand "If you haven't used the sample file" "false"  %}}
Here's the line you should copy/paste :

```ass
!retime("line",$start < 900 and -$start or -900,200)!{!$start < 900 and "\\k" .. ($start/10) or "\\k90"!\fad(!$start < 900 and $start or 300!,200)}
```

followed by <code>template pre-line all keeptags</code> in the "Effect" window (below the audio spectre) and check the "comment" box.
{{% /expand %}}

{{% notice tip "Reduce display delay" %}}
If, for a particular karaoke,  you wish for the display delay of your lines to be shorter (than 0.9 seconds), you can replace all the 900 in the line with the desired value in miliseconds (like, 500) and the \k90 by the same value divided by ten (like \k50 if you replaced 900 by 500).
{{% /notice %}}

{{% notice tip "Apply the fade-in effect without delay" %}}
If for one reason or another, you have to work on a song that doesn't have the fading effect (but has a one second delay before every line), y ou can apply this script :

```ass
!retime("line",$start < 0 and -$start or -0,200)!{!$start < 0 and "\\k" .. ($start/10) or "\\k0"!\fad(!$start < 0 and $start or 300!,200)}
```

This script will add a fade-in effect to all your lines, and only a fade-in, not a delay.
{{% /notice %}}

## Conclusion

If all is right, you get something like :

![ass](/images/creation/KaraokeDone2.png)

Blue lines marked with a `karaoke` effect are lines that you just synchronized. The white `fx` lines have been added by the effects script and have the fade-in and delay applied.

[Remove all unused styles.](../editkaraoke/removing-unused-styles)

**You have completed your first karaoke synchronization! Congratulations!** You can now [test it](../test) and share it with the world!
