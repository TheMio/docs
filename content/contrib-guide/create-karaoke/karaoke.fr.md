+++
title = "Réaliser le karaoké"
weight = 2
+++

Vous avez votre vidéo et les paroles, il est temps de se mettre au travail. Cette page va vous guider pour votre premier karaoké. Quand on parle de synchronisation, il s'agit de l'action de synchroniser les paroles sur l'audio de votre karaoké. On pourra également parler de "time" parfois.

## Le style de sous-titres

Il vous faut d'abord le *style* de sous-titres Karaoke Mugen.

[Cliquez sur ce lien](/aegisub/sampleKaraokeMugen.ass) pour télécharger le fichier exemple que nous utilisons. Faites un clic droit puis enregistrez le fichier quelque part où vous le retrouverez facilement, **vous l'utiliserez à chaque fois que vous voudrez faire un karaoké**.

## L'espace de travail

Ouvrez l'exemple avec **Aegisub**. Vous tomberez tout d'abord sur une interface quasiment vide.

![ass](/images/creation/AegisubSample.png)

- **Vous souhaitez faire un karaoké vidéo**

Allez dans **"Vidéo > Ouvrir une vidéo"**, et sélectionnez votre vidéo. Vous pouvez aussi la "drag & drop" directement sur l'interface.

- **Vous souhaitez faire un karaoké sur un fichier audio simple car vous n'avez pas trouvé de vidéo satisfaisante ou suffisamment propre (un mp3 par exemple)**

Allez dans **"Audio > Ouvrir un fichier audio"**, et sélectionnez votre fichier audio. Vous pouvez aussi le "drag & drop" directement sur l'interface.

Rendez-vous ensuite dans l'onglet **Vidéo > Utiliser une fausse vidéo**, choisissez la résolution "HD 1080p" (sauf si vous n'avez qu'une pochette d'album "classique" en 4/3 à disposition), la couleur de fond de votre choix, ainsi que la durée de la fausse vidéo (mettez plus d'images que la durée totale du mp3, ça a peu d'importance) et validez.

![ass](/images/creation/dummyvideoaudio.png)

- Après avoir intégré votre fichier, vous devriez avoir un écran similaire à celui-ci, où nous sommes en train de réaliser le karaoké de l'ending français d'*Ashita no Nadja*. Sur ce screen, si vous synchronisez un fichier audio, la seule différence que vous aurez, c'est le cadre de la vidéo qui sera entièrement remplit de la couleur que vous avez choisi pour la fausse vidéo.

![ass](/images/creation/Ecran01bis.png)

**Il peut y avoir deux différences, en rapport avec l'audio :**

1. Si vous n'avez pas la bande audio en haut à droite de l'écran, et si vous n'avez pas de son en jouant votre vidéo via le bouton ![ass](/images/creation/IcoLecture.PNG), alors le son n'est pas chargé. Remédiez-y via le menu **Audio > Ouvrir l'audio de la vidéo**.

2. Si vous n'avez pas le même type de bande audio, c'est normal ! Pour y remédier cliquez sur l'icône ![ass](/images/creation/IcoSpectre.PNG). Ce type de représentation est plus explicite.

- Une fois la vidéo **(ou l'audio)** bien intégré(e), allez dans l'onglet **Fichier** > **Propriétés** et indiquez une résolution de 0 par 0, ça vous évitera d'avoir une police trop petite affichée à l'écran. Ce réglage est à refaire ***à chaque fois*** que vous ouvrez le fichier sample, ouvrez une vidéo / un mp3, ou ouvrez un karaoké déjà fait avec Aegisub. Si vous ouvrez un fichier .ass que vous souhaitez modifier ou terminer et que vous ne faites pas ça, vous constaterez que vos lignes seront beaucoup trop petites par rapport à la dernière fois.

- Cochez également la case **"Échelle bord et ombre"**, elle permet d'afficher exactement le même style sur Karaoke Mugen **et** sur [Live](https://live.karaokes.moe).

Exemple en image :

![ass](/images/creation/ResolutionSample5.png)
![ass](/images/creation/ResolutionSample6.png)

## La synchronisation "simple" (ou *timing*)

Pour toute cette étape, on vous conseille de désactiver le **Replacement automatique du curseur vidéo au temps de début de la ligne active** en cliquant sur le bouton ![ass](/images/creation/IcoAuto.PNG) afin de faciliter votre travail.

Le but de cette étape est de synchroniser chaque *ligne* de paroles à l'écran. On ne s'occupera du syllabe-par-syllabe qu'ensuite.

***Regardez bien ces trois éléments de la "box sous-titres" :***

![ass](/images/creation/BoxSousTitres.PNG)

*(1) : le temps de début d'apparition de votre sous-titre.*

*(2) : le temps de fin d'apparition de votre sous-titre.*

Le but de cette étape, c'est de renseigner le temps de début d'apparition et de fin d'apparition du sous-titre.

Voici les étapes à suivre afin de renseigner chaque ligne :

1. Lisez votre vidéo via le bouton ![ass](/images/creation/IcoLecture.PNG) en dessous de la prévisualisation de celle-ci, et faites pause avec ![ass](/images/creation/IcoPause.PNG). Juste à droite du bouton ![ass](/images/creation/IcoAuto.PNG). Vous aurez alors l'information concernant le temps où vous avez fait pause, sous le format *heure:minute:seconde:centièmeSeconde*.
2. En étant placé sur la bonne ligne, maintenez `CTRL + 3` pour que ce temps devienne celui du début de votre ligne.
3. Reprenez ensuite la lecture et faites pause à la fin du chant de votre ligne courante.
4. En étant placé sur la bonne ligne, maintenez `CTRL + 4` pour que ce temps devienne celui de la fin de votre ligne.
5. Ajoutez le texte correspondant dans votre ligne fraichement faite.
6. Faites un clic droit en dessous de votre ligne puis **insérer (après)** pour en créer une nouvelle.
7. Sélectionnez cette nouvelle ligne et recommencez le processus jusqu'à synchroniser toutes vos lignes !

{{% notice info "Je suis Sonic le Hérisson!" %}}
Si vous progressez trop vite dans votre lecture, le bouton ![ass](/images/creation/IcoLectureLigne.PNG) vous permet de recommencer la lecture à partir du temps de début d'une ligne. Pratique si vous avez des difficultés avec le curseur sous la prévisualisation de la vidéo ! Vous pouvez aussi utiliser les flèches gauche et droite de votre clavier, qui par défaut vous font respectivement reculer et avancer d'une image au sein de votre vidéo.
{{% /notice %}}

## La synchronisation "par syllabe" (ou *synchro en k*)

Une fois toutes vos lignes synchronisées, il est temps de réaliser la synchronisation syllabe-par-syllabe de vos lignes afin qu'elles se remplissent progressivement.

Avant toute chose, on vous conseille de sauvegarder votre travail dans un nouveau fichier ("Enregistrer les sous-titres sous").

L'idée, c'est d'afficher d'abord votre ligne de sous-titre en blanc, puis de mettre les syllabes en orange au rythme de la chanson. Pour ça, on va utiliser des **balises**, ou plus précisément des balises `{\k}`.

Chaque syllabe de nos lignes de sous-titre sera donc séparée par une balise {\kX}, où "X" est le nombre de centièmes de seconde s'écoulant avant l'affichage en blanc de tout le texte suivant la balise, et ce jusqu'à la suivante (le cas échéant). Concrètement, ça signifie que la première ligne de texte va passer de `akaku somatta rashinban` à quelque chose comme `{\k11}a{\k26}ka{\k19}ku {\k24}so{\k24}mat{\k23}ta {\k11}ra{\k23}shi{\k28}n{\k23}ba{\k13}n`.

Heureusement vous n'aurez pas à mesurer avec minutie chaque centiseconde de chaque parole : Aegisub dispose d'un **Assistant Karaoké** ! Et on va l'activer immédiatement en appuyant sur ![ass](/images/creation/IcoKaraoke.PNG), la dernière icône à droite en dessus du spectre audio.

Vous constaterez alors deux changements dans votre interface, en haut à droite : votre texte apparaît maintenant mot-par-mot sur votre spectre sonore, et ce même texte prédécoupé apparaît en bas dudit spectre. **Vous remarquerez au passage qu'il est possible d'effectuer un zoom horizontal et/ou vertical du spectre via les ascenseurs à droite de ce dernier.** N'hésitez pas à en abuser selon le contexte.

![ass](/images/creation/Ecran02.PNG)

La première étape pour *synchroniser en k* une ligne via l'assistant Karaoké, c'est délimiter les syllabes. Pour ça, on va se concentrer sur notre ligne recopiée, juste en dessous du spectre audio. Si vous placez votre souris dessus, vous verrez un curseur sur le long de la case. C'est donc sur cette partie de l'écran que vous délimiterez les syllabes, en cliquant à chaque intersection vous semblant nécessaire.

Quelques règles pour se faire :

![ass](/images/creation/TableKana.png)

- Dans le cas de Karaokés japonais, on entend par "syllabe" l'équivalent d'un kana (voir tableau ci-dessus).

- Dans le cas de lettres dédoublées *(ex. : somatta)*, deux solutions selon ce qu'on entend : soit on découpe **so | ma | t | ta**  si on estime que le "*a*" est prononcé deux fois. Le "*t*" seul servira alors à marquer la 2e prononciation du a. Dans le cas inverse, si le  "*a*" est prononcé une seule fois alors on découpe **"so | mat | ta"** (et pas **"so | ma | tta"**).

- Dans tous les cas, vous devez essayer de n'avoir jamais au grand jamais un double son prononcé sur une seule découpe de syllabe. Par exemple, si c'est prononcé **"so | ma | t | ta"** mais que vous ne découpez qu'en **"so | mat | ta"**, vous allez vous retrouver avec un petit blanc pendant lequel le chanteur va doubler le son **"a"** et votre karaoké va rester sur la syllabe **mat**. C'est une erreur assez classique mais une fois qu'on a retenu le principe, ça devient un automatisme.

- On peut admettre que certaines syllabes sont parfois difficiles à distinguer les unes des autres. Ne pas hésiter, dans certains cas, à en englober deux d'un coup. Exemple : selon le tableau des hiragana / katakana, **"shin"** devrait être découpé en **"shi | n"** mais on peut admettre que non, s'il est prononcé **"shin"**, c'est à dire d'un seul bloc.

- De même, si votre chanteur *bouffe* des syllabes, vous pouvez si vous le voulez ajouter des apostrophes.
Un exemple est plus parlant :
S'il chante `kitto ashita wa` sans le "i" de "ashita", vous pouvez découper la phrase en `kit | to   | ash' | ta   | wa`

Voici un exemple de découpe :

![ass](/images/creation/ExTimeK.PNG)

- Retenez, donc, que vous n'êtes pas obligé de découper chaque syllabe si l'interprête ne les découpe pas lui aussi. C'est valable surtout pour le japonais, mais aussi pour les autres langues comme le français.

- Une fois que votre découpe vous satisfait, vous pouvez cliquer sur ![ass](/images/creation/IcoTimeOK.PNG). Si vous vous trompez sur une découpe, vous pouvez la supprimer en cliquant à nouveau dessus. Et si vous voulez effacer l'intégralité de la découpe de la ligne, cliquez sur ![ass](/images/creation/IcoTimeNOK.PNG).

- Une fois validé, vous verrez quelques changements sur l'interface : votre découpage syllabique s'est appliqué sur le spectre, et des balises **{\k}** ont été générées avec des temps plus ou moins aléatoires.

![ass](/images/creation/Ecran03.PNG)

Ensuite, il ne vous reste plus qu'à faire joujou avec les lignes jaunes pointillées du spectre pour faire correspondre *précisement* chaque zone de ce dernier à une syllabe chantées ! Cliquez et maintenez votre curseur sur l'une d'entre elles et déplacez-la à l'endroit précis où elle est chantée. Pour vous aider, vous pouvez utiliser :

- Votre oreille
- Les boutons ![ass](/images/creation/IcoLecture.PNG), ![ass](/images/creation/IcoPause.PNG) et ![ass](/images/creation/IcoLectureLigne.PNG) que vous connaissez déjà par cœur (n'hésitez pas à spammer le 3e bouton et à ré-écouter plusieurs fois les lignes jusqu'à être satisfait de votre synchronisation)
- Le spectre audio

{{% notice note "La barre blanche" %}}
La barre blanche s'affiche lors de la lecture pour vous indiquer où en est la vidéo, mais qu'elle ne reste malheureusement pas affichée lors de la pause. Il est également impossible à l'heure actuelle de lire une vidéo au ralenti pour s'aider.
{{% /notice %}}

{{% notice tip "Ecouter une syllabe en particulier" %}}
N'hésitez pas à faire un clic droit **entre** chaque ligne jaune, ça vous permet de n'écouter que la syllabe choisie, c'est très pratique. Vous pouvez aussi utiliser les flèches droite et gauche du clavier lorsque vous êtes sur le spectre audio pour bien réécouter chaque syllabe une à une et à la suite, et ajuster les lignes jaunes avec précision.
{{% /notice %}}

Il est vivement déconseillé de prendre l'habitude de faire une découpe avant la phrase. Votre ligne va apparaître et avoir du "vide" avant que la première syllabe soit chantée. Pour vous aider à ce que votre ligne démarre pile sur le début de la première syllabe sans temps mort, vous pouvez procéder comme suit :

- Faire démarrer la phrase avec de l'avance
- Placer votre 1ère ligne pointillée jaune au bon endroit
- Noter le temps où vous avez placé la ligne pointillée jaune
- Rentrer ce nouveau temps comme étant le vrai point de départ de la phrase
- Supprimer la ligne pointillée jaune

Ainsi votre phrase n'aura pas de blanc pré-chant.

![ass](/images/creation/doublebalisek.png)

Cette étape générale est longue et laborieuse, et vous ne serez pas forcément en rythme lors de vos premières réalisations, mais ne perdez pas espoir : la rapidité comme la précision viennent à force d'entraînement.

{{% notice tip "Trop d'effets à l'écran?" %}}
Si votre vidéo bouge beaucoup ou affiche trop d'éléments à l'écran pour que vous puissiez vous concentrer sur le bon timing des syllabes, vous pouvez choisir de synchroniser "par dessus", sur une fausse vidéo. Pour ça, rendez-vous dans l'onglet **Vidéo > Utiliser une fausse vidéo**, indiquez la résolution originale de votre vidéo (ou utilisez un preset dans la liste déroulante), la couleur de fond de votre choix, le fps (nombre d'images par seconde) original de votre vidéo ainsi que sa durée (mettez plus d'images qu'il n'en faut, ça a peu d'importance) et validez. Refaites la manipulation de la résolution 0 x 0 dans **Propriétés** et voilà, vous pouvez maintenant continuer à créer votre karaoké sur un fond de couleur uniforme tout en conservant l'audio de la vidéo d'origine, beaucoup plus pratique pour vérifier si votre ligne est bien timée. Pas d'inquiétude, quand vous sauvegarderez, la fausse vidéo ne sera pas prise en compte dans Karaoke Mugen.

![ass](/images/creation/FausseVideo.png)

{{% /notice %}}

{{% notice tip "Synchronisation théorique et pratique" %}}
Lors de la synchronisation, vous pouvez essayer de faire commencer chaque syllabe au moment où la première voyelle de celles-ci est prononcée (plutôt de la première lettre de la syllabe). En effet, il est plus naturel pour les spectateurs de voir une syllabe se remplir pile à ce moment.
	- Par exemple, si à un moment le chanteur prononce "My dear mother" et qu'il allonge le "M" de "mother", il n'est pas négligable de faire commencer la syllabe "mo" (de mother) pile au moment où il prononce le "o" (et non au "m").
	- Vous pouvez aussi appliquer ce principe à quasiment tout les mots, notamment ceux commençant par **ch / sh** ou **s**.
{{% /notice %}}

### Remplissage progressif

Les langues latines ayant des coupes moins franches au niveau du découpage par syllabe, il se peut que vous préfériez un remplissage "progressif" de la couleur orange sur la couleur blanche, comme les karaokés de chansons populaires que l'on peut trouver sur Internet. La balise **{\kf}** est là pour ça.

Une fois l'intégralité de votre karaoké timé selon le processus ci-dessus (en \k), vous pouvez modifier le remplissage et le passer entièrement en "kf / progressif". Pour cela, allez dans **Edition > Rechercher et remplacer**, et cherchez `{\k` (oui oui, sans fermer le crochet) pour le remplacer par `{\kf` (idem, ne fermez pas le crochet). Cliquez sur **Remplacer tout** et votre karaoké a désormais un remplissage syllabique progressif, toutes les balises \k se sont tranformées en balises \kf !

Aussi, rien ne vous empêche de mettre à la fois des balises {\k} et des balises {\kf} dans le même karaoké. Par exemple sur les génériques japonais, il arrive que les interprêtes finissent certaines de leurs phrases par une syllabe allongée, il peut être intéressant de mettre un {\kf} à ce moment-là. Attention cependant à ne pas en abuser, on considère généralement que mettre un remplissage progressif est inutile sur une syllabe allongée qui dure moins d'1 seconde, à peu près (pour l'exemple d'un kara japonais).

{{% notice info "Comment fonctionne le remplissage progressif" %}}
Par défaut, le remplissage progressif d'une syllabe se termine dès que la balise suivante est jouée (donc la syllabe suivante), or il peut arriver qu'une syllabe progressive soit terminée de chanter bien avant la prochaine syllabe.

Pour cela, n'hésitez pas à ajouter une découpe de syllabe en plus *après* votre balise kf (mais *avant* la prochaine syllabe). Vous pouvez ainsi contrôler précisement à quel moment la syllabe sera complètement remplie. Sur certaines chansons, ça peut valoir le coup.

![ass](/images/creation/doublekf.png)

*ici, la chanteuse marque une pause après "terre de"*

![ass](/images/creation/kfsyllabe.png)

*ici, la phrase sera complètement remplie juste avant de disparaître de l'écran*

{{% /notice %}}

## Changement du style d'affichage en fonction du karaoké (facultatif)

Comme vous avez pu le remarquer, le fichier d'exemple intègre plusieurs styles :

- Sample KM [Up]
- Sample KM [Down]
- Sample KM Duo [Voice 1]
- Sample KM Duo [Voice 2]
- Sample KM Duo [Voice 1&2]
- Sample KM [Choir]

**Up** est le style de ligne que vous utiliserez constamment.

Si vous créez un karaoké qui peut [se chanter à deux](https://live.karaokes.moe/?video=057ff76e-ca7b-42ba-a7cc-34b6a75315fe), les styles **Duo** sont là pour ça.

Enfin, si votre karaoké contient des voix off, il convient de les mettre soit sous le style **Choir** ([entre le milieu et le haut de l'écran](https://live.karaokes.moe/?video=8f42fecb-c15a-47d8-98da-9b7350a7f132)), soit sous le style **Down** ([en bas de l'écran](https://live.karaokes.moe/?video=fcd455f9-839d-403f-a971-88b8481ae8b0)).

Si vous souhaitez changer le style d'une ou plusieurs lignes, il suffit juste de les sélectionner puis de choisir le style correspondant.

![ass](/images/creation/ChangementStyle.png)

## Application du script

**Une règle fondamentale de Karaoke Mugen est que les sous-titres doivent être lisibles : ainsi, une ligne de texte apparaîtra environ une seconde avant qu'elle ne commence à être chantée.**

Une fois la synchronisation "en k" terminée, il faut appliquer un script, qui permettra à la fois de faire apparaître chaque ligne environ 1 seconde plus tôt et d'ajouter un effet de fondu aux phrases.

Le script est déjà inclus dans le fichier exemple que vous avez utilisé, c'est la toute première ligne que vous voyez en haut. Il suffit de l'appliquer en vous rendant dans l'onglet **Automatisme** > **Appliquer le modèle karaoké**

{{% expand "Si vous n'avez pas utilisé le fichier exemple" "false"  %}}

Voici la ligne en question si vous voulez la copier/coller

```ass
!retime("line",$start < 900 and -$start or -900,200)!{!$start < 900 and "\\k" .. ($start/10) or "\\k90"!\fad(!$start < 900 and $start or 300!,200)}
```

suivi de `template pre-line all keeptags` à rentrer dans la fenêtre "Effet" (en dessous du spectre audio), et enfin cocher la case "Commentaire"
{{% /expand %}}

{{% notice tip "Réduire le délai d'apparition des lignes" %}}
Si, pour les besoins d'un karaoké en particulier, vous souhaitez que le délai d'apparition de vos lignes soit plus court (que 0.9 seconde), il vous suffit de remplacer tous les 900 de la ligne par la valeur souhaitée en millisecondes (ex. 500) et le \k90 par la même valeur divisée par dix (ex. \k50 si vous avez remplacé les 900 par des 500).
{{% /notice %}}

{{% notice tip "Appliquer l'apparition en fondu sans ajouter le délai" %}}
Si pour une raison X, vous êtes amené à travailler sur un karaoké qui ne possède pas d'apparition en fondu (mais qui a bien environ 1 seconde d'apparition avant chaque ligne), vous pouvez appliquer ce script :

```ass
!retime("line",$start < 0 and -$start or -0,200)!{!$start < 0 and "\\k" .. ($start/10) or "\\k0"!\fad(!$start < 0 and $start or 300!,200)}
```

Ce script rajoutera à toutes vos lignes un fondu, et uniquement un fondu, pas de décalage.
{{% /notice %}}

## Conclusion

Si tout est bon, vous obtenez quelque chose comme ça :

![ass](/images/creation/KaraokeDone2.png)

Les lignes bleutées marquées avec un effet `karaoke` sont les lignes simplement timées que vous venez de faire. Les lignes en blanc `fx` qui se sont rajoutées, comme le nom l'indique, contiennent le décalage d'une seconde et le fondu d'apparition / disparition.

[Supprimez ensuite les styles inutilisés.](../editkaraoke/#supprimer-des-styles-inutilises-dans-un-fichier-de-paroles)

**Vous avez terminé votre premier karaoké ! Félicitations !** Vous êtes désormais prêt à [le tester](../test) et à en faire profiter le monde entier.
