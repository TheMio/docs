+++
chapter = true
title = "Create a karaoke"
weight = 2
+++

# Create a karaoke

Before everything: check twice that the song isn't already in the database.

- Search [this site](https://kara.moe) which compiles *all* of the available karaokes. Search to find out if your opening is actually in the database (or not).

- Search on the database's gitlab to see songs [about to be added](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?scope=all&state=opened&label_name[]=To%20Add) and [those being made](https://gitlab.com/karaokemugen/bases/karaokebase/-/issues?scope=all&state=opened&label_name[]=Doing).
- Check if the karaoke doesn't already exist in [the suggestion database](https://kara.moe/suggest/). If it does, let us know and we'll add it more quickly because the lyrics are already synchronized in another format that we can convert!

## I double-checked, there's nothing

**If your song's not in the database, you have two options:** either you submit a suggestion via [this site](https://kara.moe). Search for a song, then scroll to the bottom and use the link and fill the form to suggest us a song. Then hope that a brave soul does it for you... or make it on your own! It's simple and we're going to explain it in that tutorial.

## Create your karaoke

**NOTE : In the following sections, you'll find advice and recommendations on video size, format, trascribing japanese and other karaoke related things. These are RECOMMENDATIONS. You do NOT have to follow them if you are making karaoke songs for your own database.**

It's advised to follow the instructions in the given order.

## Table of Contents

{{% children depth="999" descriptions="true" %}}
