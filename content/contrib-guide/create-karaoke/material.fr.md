+++
title = "Réunir le matériel"
weight = 1
+++

## Installer Karaoke Mugen (optionnel)

Installer Karaoke Mugen vous permettera de vérifier comment votre karaoké rend dans le logiciel directement, un simple lecteur média suffit si vous n'avez pas le temps ou la place pour l'installer. Pour l'installation, consultez [le guide de l'utilisateur](../../user-guide/install/).

## Se procurer un logiciel de sous-titrage

Il en existe plusieurs mais chez Karaoke Mugen, on utilise tous Aegisub. Il y a néanmoins des alternatives au Aegisub de base.

### Aegisub

~~[**Site officiel**](http://www.aegisub.org)~~ Malheureusement, le site officiel est hors service pour le moment, vous pouvez [accéder au dépôt GitHub qui contient également les installateurs](https://github.com/Aegisub/Aegisub/releases/latest). Vous pouvez aussi utiliser la version Japan7 (voir plus bas)

Aegisub est plutôt simple et très complet. Il fonctionne sur Windows, MacOS et Linux (via le gestionnaire de paquets de votre distribution).

{{% notice tip "La version Portable" %}}
L'installation *portable* est utile quand vous voulez la mettre sur une clé USB et l'utiliser sur n'importe quel ordinateur sans avoir à installer quoi que ce soit. Dans le doute, prenez l'installation *complète*.
{{% /notice %}}

### Aegisub-Japan7 (BETA)

Il existe une version de Aegisub crée spécialement par des membres du [club étudiant de l'ENSEEIHT Japan7](https://www.bde.enseeiht.fr/clubs/japan7/fiche/), et qui ajoute notamment le **tap-to-time**. Cette méthode permet de faire une synchronisation "au clic", c'est à dire que vous avez uniquement à cliquer aux bons endroits dans la vidéo pour marquer les syllabes. C'est plus pratique pour les débutants mais beaucoup moins précis et il vous faudra de toute façons revoir votre synchronisation après pour en corriger les imperfections. C'est la méthode utilisée par le logiciel de karaoké de l'association Epitanime pour son format Toyunda.

Il s'agit d'une version BETA, donc il peut subsister quelques bugs !

- [Paquet AUR pour ArchLinux](https://aur.archlinux.org/packages/aegisub-japan7-git/)
- [Paquet ebuild pour Gentoo](https://git.odrling.xyz/odrling/odrling-overlay/src/branch/master/media-video/aegisub/aegisub-9999.ebuild)
- [Exe pour Windows](https://mugen.karaokes.moe/downloads/aegisub-japan7/Aegisub-Japan7-latest-x64)
- [Code source](https://github.com/odrling/Aegisub)

À vous de voir ce que vous préférez utiliser.

## Trouver une bonne source vidéo (ou audio)

- Le téléchargement de source sur YouTube n'est jamais une bonne solution, car le site compresse à mort les vidéos. Les seuls cas exceptionnels sont les vidéos uniquement trouvables dessus et vraiment pas ailleurs, dans une qualité acceptable (les vieux génériques des années 70/80 par exemple).
- Privilégiez les versions creditless (souvent notées "NC" sur les releases torrent) trouvables dans les DVD / Blu-Ray.
- La définition **minimale** pour une vidéo récente est le 720p. Une source 1080p est parfaite.

{{% notice warning "A propos des sources en 4K" %}}
Les sources 4K ne sont pas autorisées dans la base de Karaoke Mugen, pour une question de poids mais aussi pour une question de performance.
{{% /notice %}}

- *Aussi souvent que possible*, essayez de trouver une source avec une vidéo, car ça rend le karaoké plus dynamique qu'une image fixe. Si vous timez une version *full* d'un opening par exemple, peut-être qu'il existe des AMVs que vous pensez approprié pour notre affaire. Sinon, un mp3 simple fera l'affaire quand même.

**Plusieurs solutions s'offrent à vous pour trouver votre source vidéo en bonne qualité :**

### Trouver un *rip* sur les interwebs

Ceci s'applique aux karaokés ayant trait à la culture asiatique.

- Le subreddit [/r/AnimeOpenings](http://reddit.com/r/animeopenings) est une excellente source d'openings et d'endings. Par contre ils sont tous uploadés au format webm, pensez donc à changer le conteneur (via *ffmpeg* ou tout autre logiciel équivalent comme Handbrake, plus d'infos dans [Encodage](#encodage)).
- Nyaa : Vous trouverez surement votre bonheur en Blu-Ray Rip dans la catégorie "*raw*"

### Demander aux amis

Si ce n'est pas déjà fait, rejoignez le [Discord](http://karaokes.moe/discord) ou le [forum](https://discourse.karaokes.moe) et n'hésitez pas à demander aux habitués du canal *#karaoke-fr* ou la catégorie *Bases* du forum s'ils ont la chanson dans leurs bases personnelles.

Vous pouvez aussi demander à votre entourage via votre réseau social préféré, on ne sait jamais !

### Faire un rip vous-même

Encore une fois, ces recommendations sont principalement pour les karaokés liés à la culture asiatique.

Certains DVD & Blu-Ray français contiennent les génériques sans crédits, mais c'est surtout le cas sur 99% des volumes japonais. Il est possible de s'en procurer via [CD Japan](http://www.cdjapan.co.jp), en occasion sur [Mandarake](https://mandarake.co.jp) ou encore tout bêtement sur [Amazon Japan](http://amazon.co.jp).

Une fois que vous avez ça, il vous faut un lecteur DVD ou Blu-Ray pour PC, selon le support à ripper.

Pour vous permettre de lire correctement les disques et de copier-coller les flux qui vous intéressent, les logiciels conseillés sont *DVD Fab Passkey* [(site officiel)](http://fr.dvdfab.cn/passkey-lite.htm) ou *MakeMKV* [(site officiel)](http://www.makemkv.com/)

#### Avec DVD Fab

DVD Fab est un logiciel, toujours actif en arrière-plan, qui décode *à la volée* vos disques vidéo. Après que vous ayez inséré un DVD ou un Blu-Ray protégé dans votre lecteur, il mettra une dizaine de secondes à le déchiffrer.

Vous pourrez ensuite soit le lire directement dans VLC (ou autre lecteur vidéo), soit extraire les flux *m2ts* qui vous intéressent d'un simple copier-coller. Les flux *m2ts* sont situés dans `/BDMV/STREAM/` à partir de la racine de votre disque. N'hésitez pas à identifier les fichiers suivant leur taille, et même à les lire, pour trouver le bon et n'extraire que votre générique fétiche.

**Exemple :** bien que *Kazé* y ait intégré un karaoké inchantable et non désactivable, l'OP de Chunibyô est le fichier **00005.m2ts** (240Mo) du Blu-Ray français.

#### Avec MakeMKV

L'extraction de votre Blu-Ray ou DVD passe par l'appui sur un simple bouton.

{{% notice warning "A propos de la durée des vidéos sur makeMKV" %}}
Par défaut, la durée minimale d'une vidéo est considérée de 120 secondes, et se change dans les options. Comme un générique d'animé dure moins longtemps, il faudra aller modifier cette option avant de tenter une extraction sinon *MakeMKV* ne verra pas ce que vous recherchez et ne vous proposera pas d'extraire la vidéo.
{{% /notice %}}

La vidéo extraite est un  fichier `.mkv` par vidéo. *(Attention à ne pas capturer les épisodes lorsque vous sélectionnez les vidéos à extraire ! Regardez la durée qu'ils font pour deviner s'il s'agit d'un générique)*.

### Encodage

{{% notice note "Ne prenez pas tout ceci pour argent comptant" %}}
Les recommandations suivantes sont pour l'intégration dans la [base de Karaoke Mugen](http://kara.moe). Si vous ne comptez pas participer à la base de karaokés communautaire, et préférez garder vos créations pour vous, vous pouvez passer à la section suivante.
{{% /notice %}}

Le réencodage est **primordial**, car les fichiers extraits de façon brute ou téléchargés d'Internet sont très volumineux et/ou ont le mauvais encodage. Afin de garder une base de karaokés à une taille acceptable, il faut réencoder votre vidéo pour qu'elle prenne moins de place tout en gardant une bonne qualité.

On va donc **réencoder** la vidéo pour la **compresser**, et obtenir un fichier **MP4** plutôt qu'un flux *m2ts* ou *VOB*, empaqueté ou non dans un *MKV*. Il y a plusieurs écoles, mais vous pouvez jeter un œil à [MeGUI](https://sourceforge.net/projects/megui/).

Vous pouvez aussi utiliser [Handbrake](https://handbrake.fr/) ou, si la ligne de commande ne vous effraie pas, [FFMpeg](https://www.ffmpeg.org/). Un preset pour Handbrake (Options/Import From File) est disponible [ici](/handbrake/Preset_handbrake_karaoke.json).

[ShotCut](https://shotcut.org/download/) est un excellent logiciel de découpe à la frame **et qui embarque en plus** ffmpeg pour l'exportation. Quand vous avez fini de bien découper votre vidéo et que vous enregistrez le tout au format mp4, ShotCut devrait drastiquement réduire la taille de votre fichier automatiquement sans aucune incidence sur la qualité. Pour découper votre générique c'est simple, vous le déposez au milieu du soft, puis vous le déplacez à nouveau sur la timeline de montage qui se trouve en bas, vous positionnez le curseur de lecture à la frame que vous souhaitez (vous pouvez aussi vous aider des flèches droite-gauche et du timeur qui affiche la durée), puis vous faites *clic droit* > *"découper au curseur de lecture"* > *clic droit sur la partie que vous ne souhaitez pas garder* > *"supprimer"*.

#### Formats vidéos à respecter

Que vous ayez trouvé une source sur Internet ou que vous ayez effectué une extraction vous-même, vous serez **obligé** de passer par la case réencodage si votre vidéo n'est pas conforme à ce qui suit.

Afin de permettre la lecture des karaokés sur [Live](http://live.karaokes.moe) il faut que votre vidéo respecte les caractéristiques suivantes :

{{% include "includes/inc_video-format.fr.md" %}}

Pour choisir les paramètres de qualité (bitrate) vous pouvez utiliser le tableau ci-dessous :

{{% include "includes/inc_video-sizes.fr.md" %}}

Si vous n'avez pas de vidéo pour votre karaoké, **il ne faut pas faire une vidéo avec une image fixe**. Ajoutez simplement une image en pochette d'album comme expliqué [à la fin du tuto suivant](../karaoke/#ajout-dune-image-dans-le-fichier-audio).

### Ajout d'une image dans le fichier audio

{{% notice note "Note" %}}
Cette partie en plus ne vous concerne que si vous avez un fichier audio
{{% /notice %}}

{{% expand "Ajouter une pochette dans un fichier audio" %}}
Si vous n'avez que du son à disposition, il faudra tout de même mettre une image dans ses métadonnées, ce sera celle que Karaoke Mugen affichera sur le lecteur lorsque le kara sera joué.

Les petits logiciels qui permettent d'attacher des pochettes sont légion sur internet. Sur Windows, la valeur sûre qu'on vous conseille, c'est [mp3tag](https://www.mp3tag.de/download.html).

Installez-le, ouvrez-le, et allez chercher le dossier où se trouve votre MP3. Faites un seul clic gauche sur votre MP3 dans l'interface pour le sélectionner. Faites ensuite un clic droit sur le carré en bas à gauche de l'interface, supprimez l'éventuelle pochette déjà existante, puis à nouveau avec un clic droit, choisissez "Ajouter la pochette" et... eh bien choissisez votre pochette, quoi.

![ass](/images/creation/coverMP3.png)

**Une image Full HD (1920x1080) est vivement conseillée**, d'autant plus qu'avec une pochette "classique" (en 4/3), vous avez moins de place sur les côtés pour placer les phrases de votre karaoké, et vous devrez les couper en plusieurs fois pour qu'elles tiennent sur une seule ligne à chaque fois (et puis des bandes noires sur un karas, ça rend pas très bien, on est d'accord).

Une image officielle est aussi recommandée. Pour ça, [Danbooru](https://danbooru.donmai.us/posts?tags=official_art+koutetsujou_no_kabaneri) est votre ami, avec le tag `official_art` suivi de votre série / film dans la recherche. [Yande.re](https://yande.re/post) regorge aussi d'images officielles malgré l'absence du tag sur son site. Il y a aussi [Konachan](http://konachan.net/), [e-shuushuu](http://e-shuushuu.net/), [Zerochan](https://www.zerochan.net/), et... Google. Une fois vote choix fait, n'hésitez pas à rogner / redimensionner votre image au besoin.

Si vous ne trouvez pas de bonne image, vous pouvez vous rabattre sur la pochette de l'album / du single de votre kara. Dans ce cas-là, vous pouvez en plus, si vous le souhaitez, utiliser CoverMagick pour créer un joli wallpaper 16/9 de votre pochette 4/3. Pour ça rendez-vous sur [ce lien](/CoverMagick), uploadez-y votre pochette, cochez la case "Télécharger en zip" puis cliquez sur "Go" pour récupérer le résultat.

![ass](/images/creation/covermagick.jpg)

{{% /expand %}}

## Trouver les "vraies" paroles

Sujet plus délicat qu'il n'y paraît, tant les transcriptions à l'oreille foisonnent sur Internet. Si plusieurs sources fiables sont d'accord entre elles, on peut se dire que c'est bon.

Voici nos références en général :

{{% include "includes/inc_lyrics.fr.md" %}}

Autrement, il va falloir se débrouiller seul, en récupérant le livret officiel du CD où la chanson est passée et retranscrire les paroles vous-mêmes en romaji. Cela peut être long si vous n'êtes pas habitué, mais reste faisable en vous aidant de ce tableau :

![Table](/images/creation/TableKana.png)

Si vous ne reconnaissez pas un kanji, le site [Japanese character recognition](http://maggie.ocrgrid.org/nhocr/) peut vous aider à le "numériser" pour le passer dans Google Traduction ou autre. Vous pouvez aussi [passer sur le Discord](https://karaokes.moe/discord) et demander à nos quelques membres qui sauront vous aider à traduire.

Gardez également à l'esprit les règles de transcription (que vous trouverez dans [Références](../references)).

{{% include "includes/inc_transcript-rules.fr.md" %}}

## J'ai tout le matos !

Une fois que vous avez Aegisub, la vidéo (ou l'audio), les paroles, il est temps pour vous de [créer le karaoké](../karaoke) avec tout ça.
