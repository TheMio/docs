+++
title = "Envoyer votre karaoké"
weight = 4
+++


## Je ne souhaite pas m'investir trop ni devenir mainteneur

- Vous pouvez passer par [le formulaire d'envoi](https://kara.moe/import).
    - Ce formulaire accepte les fichiers karaoké au format ASS (conseillé), Ultrastar (txt), KAR et Karafun (kfn). Pour tout autre format que ASS, le fichier de karaoké sera converti automatiquement.
    - Cette page consiste en la création du **.kara.json**, avec son média et son **.ass** en pièce jointe. Le tout sera ensuite envoyé dans une boîte de réception où un membre de l'équipe de mainteneurs se chargera de valider et d'intégrer votre karaoké dans la base commune. **Cela peut prendre du temps.**
    - Une issue sera également automatiquement ouverte [ici](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=ToDo), permettant de vous tenir au courant si votre kara a bien été ajouté (l'issue sera alors fermée) ou s'il y a des modifications à apporter (en commentaire).
    - Ce formulaire accepte les fichiers karaoké au format ASS (conseillé), Ultrastar (txt), KAR et Karafun (kfn). Pour tout autre format que ASS, le fichier de karaoké sera converti automatiquement.
    - Cette page consiste en la création du `.kara.json`, avec son média et son `.ass` en pièce jointe. Le tout sera ensuite envoyé dans une boîte de réception où un membre de l'équipe de mainteneurs se chargera de valider et d'intégrer votre karaoké dans la base commune. **Cela peut prendre du temps.**
    - Une issue sera également automatiquement ouverte [ici](https://gitlab.com/karaokemugen/karaokebase/issues?label_name%5B%5D=ToDo), permettant de vous tenir au courant si votre kara a bien été ajouté (l'issue sera alors fermée) ou s'il y a des modifications à apporter (en commentaire).
- Vous pouvez également passer sur [le Discord](http://karaokes.moe/discord) pour tout nous envoyer à la mano.

## Je veux ajouter moi-même mes chansons pour pas vous embêter et devenir mainteneur

C'est gentil !

Après nous avoir contacté [sur Discord](https://karaokes.moe/discord) on peut vous ajouter comme "Mainteneur" sur la base de karaokés et vous aider à configurer votre dépôt **kara.moe** dans le panneau système.

Vous aurez besoin de [télécharger Git](https://git-scm.com/downloads) pour votre système d'exploitation et de l'installer. Normalement la configuration par défaut de Git est suffisante.

Maintenant éditez la configuration du dépôt **kara.moe** dans votre application (onglet Dépôts du panneau système)

- "Mode Mainteneur" doit être activé.
- Les options git doivent être renseignées :
    - **URL** : `https://gitlab.com/karaokemugen/bases/karaokebase.git`
    - **Username** : Votre nom d'utilisateur sur Gitlab
    - **Passowrd** : Jeton d'accès Gitlab que vous trouverez dans le menu "Préférences" > "Jeton d'accès" de votre profil. Le jeton a besoin des droits "Write repository"
    - **Identité Git** : Votre nom tel qu'il est sur votre profil Gitlab
    - **Email Git** : Même chose avec l'email
    - **Hôte FTP** : `erin.mahoro-net.org`
    - **Port FTP** : `21`
    - **User FTP** : Il vous sera communiqué par nos soins
    - **Password FTP** : Pareil.
    - **Dossier de base FTP** : `storage/medias-otaku`

Une fois votre dépôt configuré correctement (cela peut prendre un peu de temps) vous pourrez accéder au menu "Mainteneur" du panneau système et cliquer sur "Git". D'ici, vous pouvez "Pousser" vos modifications vers le dépôt en un clic. KM s'occupera d'envoyer votre vidéo et ensuite les fichiers sur le dépôt.

Une série de tests automatique est effectuée par Gitlab pour vérifier si votre modification est bonne. Si ce n'est pas le cas, un message d'erreur s'affichera sur Discord dans le canal *#git-bases*. Vous pouvez également [consulter les derniers **pipelines**](https://gitlab.com/karaokemugen/karaokebase/pipelines) pour voir si le test s'est bien passé ou pas.

- Si le test ne s'est pas bien passé, ne paniquez pas : quelqu'un vous aidera à y voir plus clair dans le message d'erreur affiché via la console du pipeline.
- Corrigez l'erreur ou demandez à quelqu'un de la corriger pour vous si vous ne comprenez pas.
- Relancez le pipeline ou faites un autre commit.

{{% notice info "Plus d'informations sur Git" %}}
Git est un logiciel de gestion de versions. C'est l'outil le plus adapté pour gérer une base de données comme celle-ci, car il permet d'avoir un journal de qui à modifié quoi, quand, et d'être absolument certain d'avoir exactement la même base de karaokés que quelqu'un d'autre.

Chaque modification est archivée et peut être retrouvée. À tout moment, on peut également revenir en arrière si l'on s'aperçoit qu'on a fait une bêtise. Plus d'infos :

- [C'est quoi Git](http://putaindecode.io/fr/articles/git/) ?
- [Tutoriel sur Git](https://openclassrooms.com/courses/gerez-vos-codes-source-avec-git).
- Des clients graphiques git qui sont bien :
    - [GitAhead](http://gitahead.com) est un outil graphique pour gérer son dépôt Git. (gratuit)
    - [Fork](http://git-fork.com) est un excellent outil mais payant.
{{% /notice %}}
