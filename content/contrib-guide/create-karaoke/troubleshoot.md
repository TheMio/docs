+++
title = "Troubleshooting"
weight = 8
+++

## .kara.json files

### Tools

**.kara.json** files must all be created and filled automatically by [this form](../../karafile) in the system panel.

#### Karaoke Mugen data generation

In the case you modify media files or lyrics, you can update `.kara.json` files in several ways.

- **From a karaoke page in the system panel** : drag and drop your new media or lyrics file in the form and save changes so karaoke Mugen will create a new **.kara.json** and replace the old one.
- **From the system panel** : in the *Database* menu click on *Validate*
- **When Karaoke Mugen starts** with the `--validate` option, the software will check if your files were changed. If they have changed, they will be updated and karaoke Mugen will exit.

### Common issues

#### Video file can not be found despite being present

Verify on the FTP that the file is named exactly like in the **.kara.json**. Beware of case! Since Windows is case-insensitive, it won't have any trouble, but for Linux where the lab is hosted, it will cause issues. Look carefully.

Another possibily is an invisible parasite character, a double space or something other from that. In doubt, copy-paste the file name from the **.kara.json** while renaming you media file

## .ass lyrics files

### Your subtitles are shifted/unsynced

Look up the [time shifting section from the advanced guide](./karaoke-advanced.md#convert-subtitle-speed)

## Medias

### Tools

To encode videos, an array of tools exists.

- **Handbrake** is the easiest to use, with a relatively correct user interface that is readable and somewhat good default options.
- **MediaCoder** is a bit more shaky but is full of extra stuff: like the ability to keep some media tracks and re-encode other ones. Beware, it is a *freemium*: some options are only usable with the software's full version.
- **MeGUI** is a lot more harder to use but opitimizes encoding.
- **FFMPEG** can be used in command line, making it very powerful but hard to master.
- **ShotCut** allows to cut videos at the exact frame you want and uses FFMPEG, which makes your exports lighter.

### Common issues

#### False metadata

In rare cases the "duration" can be wrong compared to reality. If it happens, it's possible to get rid of the problem by recalibrating the video track and the incriminated audio in an MP4 container, using ffmpeg and the following command line [(see the dedicated section)](../soft-advanced/#fixing-duration)

#### "Crushed" lyrics on Karaoke Mugen

If you see that lyrics at the top of the screen seem to be crushed compared to other karaokes, or that letters are skipped, then the video has a bad ratio indicated in its metadata. See the capture below:

![ass](/images/creation/crushedSubtitle.jpg)

You will have to re-encode the video with scale correction.

To find the values required for re-encoding, you will have to look in the mpv console, or use ffmpeg. For example, using mpv:

```
VO: [vdpau] 720x576 => 1024x576 vdpau[yuv420p]
```

Here, mpv indicates that the video is stretched in width, the width increases from 720 to 1024 pixels.

To fix the video, use ffmpeg and the following command, replacing the 1024:576 values with the values given by mpv.

```
./ffmpeg -i video_source.mp4 -c:a copy -c:v libx264 -vf "scale=1024:576" video_destination.mp4
```

For further explanation, [see the dedicated section](../soft-advanced#fixing-ratio)

#### Very big video files

Please use the tools and check rules on this page to produce light video files depending on their initial resolution. Sometimes re-encoding will be needed.

As a reminder, here are the expected video formats and the length/size ratio of videos:

{{% include "includes/inc_video-format.md" %}}

{{% include "includes/inc_video-sizes.md" %}}
