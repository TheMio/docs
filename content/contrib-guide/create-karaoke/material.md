+++
title = "Gather Materials"
weight = 1
+++

## Install Karaoke Mugen (optional)

Installing Karaoke Mugen will allow you to check how your karaoke looks like directly in the app. However, a simple media player should suffice if you don't have enough space to install it. Check out the [user guide](../../user-guide/install/) for more information.

## Get subtitlting software

There are several of those on the Internet, but at Karaoke Mugen, we all use Aegisub. There are some alternative to the basic, vanilla Aegisub.

### Aegisub

~~[**Official Site**](http://www.aegisub.org)~~ Unfortunately, the official website is not available at the moment, you can instead [go to GitHub](https://github.com/Aegisub/Aegisub/releases/latest). You can also use the Japan7 version (see below)

It's rather simple and full of features. It's working on Windows, MacOS and Linux (via your package manager).

{{% notice tip "The portable version" %}}
The *portable* version is useful when you want to put Aegisub ona  USB key or other removable media and use it on any computer you're on without having to install anything. If you don't know, pick the *full* installer.
{{% /notice %}}

### Aegisub-Japan7 (BETA)

There is another version of Aegisub created by the [Japan7 ENSEEIHT student club](https://www.bde.enseeiht.fr/clubs/japan7/fiche/) which fixes some bugs and adds a few features like a **tap-to-time** mode. This method allows you to synchronize lyrics *by clicking*, which means that you only have to click at the right spots in the video to mark syllables. This is easier for beginners but also much less precise and you'll need to review your synchronization after that to fix all little mistakes. This is the method used by the syncing software from Epitanime and it's Toyunda format.

This is BETA, so it can contain a few bugs still!

- [AUR package for ArchLinux](https://aur.archlinux.org/packages/aegisub-japan7-git/)
- [ebuild package for Gentoo](https://git.odrling.xyz/odrling/odrling-overlay/src/branch/master/media-video/aegisub/aegisub-9999.ebuild)
- [Windows executable](https://mugen.karaokes.moe/downloads/aegisub-japan7/Aegisub-Japan7-latest-x64)
- [Source code](https://github.com/odrling/Aegisub)

Pick the one you wish to use.

## Find a good video (or audio) source

- Downloading a video source on YouTube is never a good solution since the site compresses video a lot. Of course you can still use YouTube if you can't find the video anywhere else in acceptable quality (for example, anime songs from the 70s or 80s)
- Try to find creditless versions (often found with the "NC" tag on torrent releases) which are only available on DVD or Blu-rays.
- Minimum resolution for a video should be 720p. A 1080p source is obviously preferred.

{{% notice warning "About 4K sources" %}}
No 4K video is allowed in the official Karaoke Mugen base to save on precious space.
{{% /notice %}}

- Try to find a video source instead of an audio one *as often as possible*.  This makes your karaoke more dynamic than a still image. If you time a *full* version of an opening, maybe a music video exists and can be better than an audio file.

**There are several ways for you to find a good, quality video :**

### Find a *rip* on the Internet

These are only for asian-related karaokes.

- The [/r/AnimeOpenings](https://reddit.com/r/animeopenings) subreddit is an excellent source for openings and endings. However they alwayse use the *webm* container, so you'll have to convert it to *mp4* using *ffmpeg* or *Handbrake*. More info in the [encoding section](#encoding)
- Nyaa : You'll surely find what you seek in the *raw* category.

### Ask your friends

If you haven't done that yet, join our [Discord](https://karaokes.moe/discord) or our [forum](https://discourse.karaokes.moe) and don't hesitate to ask people on the #karaoke channel (Discord) or the Bases category on the forum if they don't have the video you seek already somewhere.

You can also ask people you know or your followers on social media. You never know!

### Make the rip yourself

Again, this is aimed primarily for asian karaokes.

Some DVD or Blu-Ray contain creditless openings and endings, especially the japanese ones. You can find new ones [CD Japan](https://www.cdjapan.co.jp), used ones on [Mandarake](https://mandarake.co.jp) or even by checking out [Amazon Japan](https://amazon.co.jp).

Once you have your disc, you'll need a DVD or Blu-Ray drive on your PC.

To be able to play discs and copy the bits you want, you'll need either *DVD Fab Passkey* [(official site)](https://dvdfab.fr/passkey-lite.htm) or *MakeMKV* [(official site)](https://www.makemkv.com/)

#### With DVD Fab

DVD Fab is a software running in the background which decrypts your video discs *in real-time*. After you insert a protected DVD or Blu-Ray in y our drive, it'll need about ten seconds to decrypt it.

You can then either play the disc directly in VLC (or any other video player), or extract the *m2ts* streams you want with a simple copy and apste. *m2ts* streams are in the `/BDMV/STREAM/` folder on your disc. You can identify files depending on their size. Of course, play them to make sure they are what you think they are.

#### With MakeMKV

Extracting your Blu-Ray or DVD can be done with the single press on a button.

{{% notice warning "About MakeMKV and minimal video length" %}}
By default, the minimal video length is 120 seconds, which can be modified in the settings. Since an anime opening is shorter than that, y ou'll have to modify that setting before trying to extract stuff from the disc or else *MakeMKV* will simply ignore those.
{{% /notice %}}

The extracted video is a `.mkv` file. Make sure not to extract whole episodes when you select which videos to extract. Check out their duration to guess if it's an opening/ending or not.

### Encoding

{{% notice note "Do not take everything here into account" %}}
These requirements are for when you want to add your song to the [Karaoke Mugen repository](http://kara.moe). If you want to keep it to yourself, you don't have to follow these guidelines and can jump to the next section.
{{% /notice %}}

Re-encoding is **necessary**, since extracted files are quite big by default. In order to keep a karaoke base at an acceptable size, you need to encode your video to make it smaller while still keeping a good video quality.

Once the correct video stream has been extracted, we'll need to **reencode** it to **compress** it, and get a *mp4* file instead of a *m2ts*, *vob* or *mkv* file. There are several ways to do so, but you can check out [MeGUI](https://sourceforge.net/projects/megui/) for that.

You can also use [Handbrake](https://handbrake.fr/) or, if you're not scared by the command line, [FFMpeg](https://www.ffmpeg.org/). A handbrake preset (Options/Import From File) is available [here](/handbrake/Preset_handbrake_karaoke.json).

[ShotCut](https://shotcut.org/download/) is also an excellent software to cut a video by the video frame. It also includes ffmpeg. When you've finished cutting your video and save it to the mp4 format, ShotCut should drastically reduce your video file size without any quality drop.

In order to cut a video, drop it in the middle of the software's window, place the cursor where you want it (you can also use the left and right arrows for that), and then *right click* and select *"cut at the playing cursor"* then *right click on the part you don't want to keep* and then *"delete"*.

### Video formats required

Wether you've found your source on the Internet or extracted it yourself, you'll have to reencode your video if it doesn't meet the following requirements.

In order to allow karaokes to be played on [Live](http://live.karaokes.moe) your video needs to have these settings :

{{% include "includes/inc_video-format.md" %}}

To decide on which quality settings to use (bitrate and such), use the following table:

{{% include "includes/inc_video-sizes.md" %}}

If you don't have any video for your karaoke, **do not make a video with a still picture**. Simply add an image as album cover art in your mp3 file as explained next.

## Add a cover art to your audio file

{{% notice note "Note" %}}
This additional part is only if you have timed an audio file
{{% /notice %}}

{{% expand "Add a cover image to an audio file" %}}
Once the MP3 is synchronized correctly and scripts have been applied, you have to add a cover art picture in [its metadata](https://tinyurl.com/y3g9f9b6) that Karaoke Mugen will dipslay on screen when the kara will be played

There are a lot of software available to do this. On Windows you can use [mp3tag](https://www.mp3tag.de/download.html).

Install and open it, and go find the folder where your MP3 file is. Click once on it in the user interface to select it. After that make a right click on the square in the bottom left corner and delete the current album cover if there is already one. Right-click again, then select "Add a cover" and pick one on your computer.

![ass](/images/creation/coverMP3.png)

**A full HD (1920x1080) picture is recommended**, If you use a classic 4/3 cover, your lyrics will have less space to display on screen and will cause double lines or worse, and you'll need to cut them several times to fit into the 4/3 aspect ratio. Besides, it'll display black borders left and right on the screen.

An official picture is also recommended. You can find some on [Danbooru](https://danbooru.donmai.us/posts?tags=official_art+koutetsujou_no_kabaneri) by specifying the `official_art` tag followed by your series / movie in the search field. [Yande.re](https://yande.re/post) also has plenty of official pictures in spite of its lack of tags. There are also [Konachan](http://konachan.net/), [e-shuushuu](http://e-shuushuu.net/), [Zerochan](https://www.zerochan.net/), and... Google. Once you found one, don't hesitate to crop/resize the picture if needed.

If you don't find a good picture, use the album/single cover art your karaoke is from, even if it's 4/3. In this case, you can use CoverMagick to create a nice 16/9 wallpaper with your 4/3 cover art. To do this, use [this link](/CoverMagick) and upload your art there, click on the "Download as zip" and click Go to get the result.

![ass](/images/creation/covermagick.jpg)

{{% /expand %}}

## Find the "real" lyrics

This is much more difficult than it seems, since transcripts by ear are often found on the Internet. If several sources seem to agree on one way to write things, you can tell it's a good transcript.

Here are our usual references:

{{% include "includes/inc_lyrics.md" %}}

If you can't find anything, you'll have to do it on your own, by finding the official booklet from the CD single or album, and transcript the lyrics yourself in romaji. This can take some time if you're not used to it, but this is still doable if you use this table:

![Table](/images/creation/TableKana.png)

if you don't recognize a kanji, the [Japanese character recognition](http://maggie.ocrgrid.org/nhocr/) site can help you "digitize" it to put it in Google Translate or something similar. You can also [come on our Discord](https://karaokes.moe/discord) and ask us for help.

Also keep in mind the following transcription rules:

{{% include "includes/inc_transcript-rules.md" %}}

## I've got everything!

Once you have Aegisub, a video (or audio) and lyrics, it's time for you to [create a karaoke](../karaoke).
