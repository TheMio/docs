+++
title = "References"
weight = 9
+++

This section is relevant only if you wish to send your song to the official Karaoke Mugen database. If it's for yourself, you're free to do as you wish.

You might need this information to create your karaokes :

## Websites

When you edit or add a song, it's important to have good references so not to make a mistake when naming artists, singers, songwriters, studios, etc.

Beware, some of them use a particular writing style (with quotes or other special characters).

{{% include "includes/inc_series-names.md" %}}

We are of course [aware](https://gitlab.com/karaokemugen/karaokebase/issues/417) that sometimes, some information is difficult to find, (AMV authors from the 2000s, songwriters, etc.) We won't whip in public if you can't find everything.

## Lyrics

{{% include "includes/inc_lyrics.md" %}}

## Japanese

{{% include "includes/inc_japanese.md" %}}

## Video size table

{{% include "includes/inc_video-sizes.md" %}}

## Studios / creators

Nock published a Google Sheet containing the most exhaustive list of creators and animation studios.

Almost every studio in there are already in the Karaoke Mugen database.

This list should be used as a reference, if you contribute to another database than KM's, please keep that list in mind so your karaokes and those from KM's base use the same studio/creator names.

[The list is available here](https://docs.google.com/spreadsheets/d/1ULoVCi7UvTG0qSMVUhnPLOOiehH5jVrVYZMsuYi9O7s/edit#gid=18648527)

## Transcript rules

{{% include "includes/inc_transcript-rules.md" %}}
