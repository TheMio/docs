+++
title = "Other Advanced Tasks"
weight = 7
+++

While you will handle karaoke files you will potentially  encounter the need to perform tasks which are somewhat complex. We are listing some of those to help you.

{{% notice warning "Warning" %}}
If you struggle with command lines, it would be advisable to put that to later.**
{{% /notice %}}

## Install tools

You will need some tools to continue..

### Bash

**Bash** is, in layman’s terms, a windows console but more powerful, and has an advanced scripting system compared to the traditional Windows (CMD.exe)

With Linux or macOS Bash or zsh are the default shells.

For windows users, you can [download it here](https://gitforwindows.org/). Once the software is installed, all you need to start is to go inside a folder of your choice with the file explorer and right click in an empty space and then select **Git Bash**.

With windows 10, an alternative is to use the [Linux subsystem](https://docs.microsoft.com/en-us/windows/wsl/install-win10) that enables you to use bash and install ffmpeg easily.

### ffmpeg

**ffmpeg** is used only via command line (even if some versions with user interfaces are availaible).

[Download the latest version for your OS on the official website](http://ffmpeg.org). A numbered version with static *linking* will be enough. Note that if you use a package system you will easily find ffmpeg there.

- For Windows : [Chocolatey](http://chocolatey.org)
- For macOS : [Homebrew](http://brew.sh)
- For Linux : It depends on your distributions, but some package a very old version of ffmpeg.

#### Usage example under Windows

When you’ll need to apply a script line to one or several files, all you will have to do is:

- Copy the file in the folder along with **ffmpeg.exe**
- Open **(Git) Bash** by right clicking into the  directory then select **Git Bash Here**
- Write the Git Bash script line that you want to use and then hit enter.

## Video

### Changing the video container (for example convert from .avi to .mp4)

```sh
./ffmpeg -i "YourOriginalVideo.avi" "out.mp4"
```

ffmpeg allows you to change a video container easily, but you can also use this to convert a video to an mp3.

### Convert a lot of videos at once

webm to mp4 for example

```sh
ls *.webm >webm.txt
```

then

```sh
while read file ; do f=`basename "$file" .webm`; ./ffmpeg -i "$file" "$f.mp4" ; done < webm.txt
```

### Extract subtitles from a mkv

```sh
./ffmpeg -i "YourVideo.mkv" "out.ass"
```

### Extract subtitles from a lot of mkvs

```sh
ls *.mkv >mkv.txt
```

then

```sh
while read file; do f=`basename "$file" .mkv`; ./ffmpeg.exe -i "$file" "$f.ass" ; done < mkv.txt
```

### Fixing duration

You need to re-encapsulate the media like this:

```sh
ffmpeg -i video_source.mp4 -vcodec copy -acodec copy video_destination.mp4
```

### Fixing ratio

A video may display correctly, but karaoke is stretched or letters are missed when filling in. This is usually a ratio problem.

ffmpeg or ffprobe indicate this ratio, the SAR (Sample Aspect Ratio).

For example:

```sh
ffprobe kochoneizer.mp4
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'kochoneizer.mp4':
Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.76.100
Duration: 00:01:31.76, start: 0.000000, bitrate: 1843 kb/s
Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p(tv, smpte170m/bt470bg/bt470bg), 720x576 [SAR 64:45 DAR 16:9], 1710 kb/s, 25 fps, 25 tbr, 12800 tbn, 50 tbc (default)
    Metadata:
    handler_name    : VideoHandler
    vendor_id       : [0][0][0][0]
Stream #0:1(und): Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 127 kb/s (default)
    Metadata:
    handler_name    : SoundHandler
    vendor_id       : [0][0][0][0]
```

Look in the Stream Video line and find something like `720x576 [SAR 64:45 DAR 16:9]`.

The SAR is set to 64:45, for a resolution of 720×576. The DAR (Display Aspect Ratio) indicates the format (16:9 video).

The SAR parameter indicates the shape of a pixel. In video, a pixel is not necessarily square.

With the SAR set to 1:1, the pixel is square. Otherwise, a pixel is rectangular. For example, the SAR set to 64:45 indicates that the pixel is rectangular.

![ass](/images/creation/SAR-11.png)

SAR defined at 1:1. The pixels are squared.

![ass](/images/creation/SAR-64-45.png)

SAR set to 64:45. The pixels are rectangular.

Why using this SAR parameter? For various reasons.

- Use of the 16:9 format by stretching the pixels in width; extremely common practice with DVD videos.
- Reduce bandwidth usage during TV broadcasting (DVB-T), where frequently the video is broadcast in 1440×1080 and stretched (with SAR) in 1920×1080.


To calculate the output resolution, it's very simple:

```
width × SAR.
```

So 720 × (64 ÷ 45), giving 1024.

And if we divide 1024 by 576, we find 16/9 (16:9), the DAR value.

To correct the ratio of the video, it will be necessary to re-encode it using the vf scale parameter of ffmpeg and using the width calculated above:

```sh
ffmpeg -i kochoneizer.mp4 -c:a copy -c:v libx264 -vf "scale=1024:576" kochoneizer-1024.mp4
```

{{% notice note "Note" %}}
SAR is a name in ffmpeg. In other software, it is called PAR (Pixel Aspect Ratio). This refers to the same thing.
{{% /notice %}}
