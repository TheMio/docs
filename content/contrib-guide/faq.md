+++
title = "FAQ"
weight = 7
+++

## Error type: "Media file not found"

Make sure the video has the exact name it should have. be careful about case since the deployment tools and database are case-sensitive, especially if using Windows.

Then make sure that there are no double spaces.

If you're absolutely sure that it's okay, but it's still not working, rename the file with another name then rename it back to its current name: some parasite characters may sometimes hide and appear as spaces, but aren't considered as real spaces!
