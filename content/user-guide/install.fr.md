+++
title = "Installation"
weight = 1
+++

Cette page décrit comment récupérer et installer Karaoke Mugen.

**NOTE :** Une fois installé, Karaoke Mugen ne nécessite pas de connexion internet, sauf pour certaines fonctionnalités :

- L'accès à distance pour le public via l'adresse https://xxxx.kara.moe
- Les comptes en ligne
- La mise à jour de la base de karaokés au démarrage
- La liste des karaokés les plus populaires
- La lecture des karaokés **qui n'ont pas été téléchargés au préalable**

Vous devriez néanmoins lancer Karaoke Mugen une première fois avec un accès Internet afin de récupérer la dernière version de la base de données de chansons.

## Télécharger Karaoke Mugen

Des exécutables pour Windows, macOS et Linux sont disponibles. Vous trouverez soit des installeurs, soit des archives portables. **Si vous ne savez pas quoi prendre, téléchargez l'installeur.**

Si vous n'avez pas déjà téléchargé Karaoke Mugen, [consultez la page de téléchargement du site](http://mugen.karaokes.moe/download.html).

## Installer Karaoke Mugen

Plusieurs façons d'installer sont possibles, via des versions compilées prêtes à l'emploi ou les sources (pour les utilisateurs plus avancés.)

Une fois que c'est fait, vous pouvez passer à la section [démarrage rapide](getting-started.md)

### Windows

Deux possibilités :

- Installeur : Double-cliquez sur l'exécutable et suivez les instructions.
	- Les données de Karaoke Mugen seront stockées à la racine de votre dossier utilisateur (dossier `KaraokeMugen`)
- Archive portable : Décompressez l'archive .zip dans un dossier où vous avez de la place.
	- Vous pouvez très bien mettre l'application portable sur une clé usb, toutes les données seront stockées sur celle-ci. C'est utile si vous voulez avoir votre application avec vos playlists de façon nomade.

### macOS

Il vous suffit juste d'ouvrir l'image DMG et de glisser Karaoke Mugen vers le dossier Applications.

### Linux

Peu importe votre distribution de choix, nous vous recommandons d'utiliser l'**AppImage** qui est normalement agnostique.

Le support Linux, bien que présent, est soumis à de nombreux aléas (chaque distribution et installation Linux est différente). Si vous rencontrez des problèmes, n'hésitez pas à [venir nous voir](../about/contact.md).

#### Paquet Debian (.deb)

Le paquet devrait créer une entrée dans votre menu d'applications.

#### ArchLinux

Il existe deux paquets AUR, [`karaokemugen`](https://aur.archlinux.org/packages/karaokemugen) et [`karaokemugen-git`](https://aur.archlinux.org/packages/karaokemugen-git/). Le premier est basé sur la dernière version en date, le second est basé sur le dernier commit de la branche `master`. La méthode d'installation reste la même peut importe celui que vous choisissez.

_N'importe quel empaqueteur AUR devrait fonctionner, ici nous utiliserons `pikaur`._

##### Installation

```Shell
$ pikaur -S karaokemugen-git
```
![Capture d'écran de Pikaur](../img/setup/km40.jpg)

_L'installation peut prendre du temps selon votre configuration, de 10 à 20 minutes._

Une fois l'installation finie, votre console devrait ressembler à ça :

![Capture d'écran de Pikaur, à la fin de l'installation](../img/setup/km41.jpg)

_Si vous voyez ça, c'est que tout s'est bien passé !_

##### Configuration initiale

Comme vous avez pu le lire, le paquet AUR est fourni avec un utilitaire, `karaokemugen-install`, permettant la configuration automatique de Karaoke Mugen ! Lancez cet utilitaire dès que l'installation est terminée.

```Shell
$ karaokemugen-install
```

**Attention :** comme indiqué au lancement, ce script risque fort de ne pas fonctionner si vous avez changé la configuration de PostgreSQL. Ce script va nécessiter les droits _super-utilisateur_ à plusieurs reprises à l'aide de _sudo_.

Au lancement, le script vous avertit d'éventuels problèmes qui pourraient empêcher le bon déroulement de l'installation et effectue ensuite toutes les actions nécessaires pour prévoir le fonctionnement de Karaoke Mugen (créer la base de données, appliquer la configuration nécessaire, etc...).

Ce script est interactif, il se peut que le script vous pose quelques questions sur les actions à adopter (notamment si une base de Karaoke Mugen est détectée) ou sur certains éléments de configuration.

![Capture d'écran de karaokemugen-install](../img/setup/km42.jpg)

Une fois le script terminé, vous êtes prêts pour lancer Karaoke Mugen.

##### Lancement

Bravo ! Votre installation de Karaoke Mugen est désormais prête à être lancée pour vivre de formidables aventures.

Vous devriez pouvoir trouver Karaoke Mugen dans le menu Applications de votre environnement de bureau, si ce n'est pas le cas, lancez simplement `karaokemugen` dans un terminal.

![Capture d'écran du lanceur d'applications](../img/setup/km44.png)

![Capture d'écran de l'application Karaoke Mugen](../img/setup/km43.jpg)

##### Ça ne marche pas ?

Tout d'abord, désolé, nous voulons que l'installation soit la plus simple possible mais nous ne pouvons pas tout prévoir. N'hésitez pas à [venir nous voir](../about/contact.md) si vous avez le moindre souci.

## Installer Karaoke Mugen depuis les sources

Vous pouvez installer Karaoke Mugen en téléchargeant son code source.

- Téléchargez une version de Karaoke Mugen depuis [le dépôt Git](https://gitlab.com/karaokemugen/karaokemugen-app) via la commande `git clone` ou via l'archive ZIP correspondante.
	- Chaque version est étiquetée (tag) : prenez la [version qui vous intéresse](https://gitlab.com/karaokemugen/karaokemugen-app/tags).
	- Si vous aimez le risque, vous pouvez [télécharger la version en cours de développement](https://gitlab.com/karaokemugen/karaokemugen-app/repository/next/archive.zip). **Attention, elle contient très probablement des bugs, mais aussi des nouvelles fonctionnalités incroyables, _choose your poison_ !**
- Téléchargez [mpv](http://mpv.io) et placez son exécutable dans le dossier `app/bin` ou indiquez son chemin dans le [fichier de configuration](../advanced/#configuration) si vous avez déjà mpv d'installé sur votre système.
	- Version 0.33 minimum requise
- Téléchargez [ffmpeg](http://ffmpeg.org) et placez l'exécutable `ffmpeg` dans le dossier `app/bin`.
	- Version 3.3.1 minimum requise
- Téléchargez [nodeJS](https://nodejs.org/en/download/) qui est nécessaire pour faire tourner Karaoke Mugen.
	- Version 16 requise. Ne téléchargez pas de version majeure plus récente.
	- Installez nodeJS une fois téléchargé.
- Téléchargez [GNU Patch](https://savannah.gnu.org/projects/patch/) si votre distribution n'a pas la version requise (voir en dessous.) C'est requis pour gérer les patches de la base de donénes de chansons.
    - Version minimale requise : 2.7
- Téléchargez la version de [PostgreSQL](http://postgresql.org) correspondante à votre système.
    - Version 13.x minimum requise.
	- Soit vous placez le contenu de l'archive des binaires dans `app/bin/postgres` de façon à avoir un dossier `bin`, `share`, `lib`, etc. dans `app/bin/postgres` soit vous utilisez votre propre serveur PostgreSQL
	- Si vous voulez utiliser votre propre serveur PostgreSQL (ou si vous êtes sur Linux), consultez la [partie configuration](../advanced/#database). Consultez le README de l'application pour savoir quelles commandes passer pour configurer la base de données.

Si vous n'avez pas déjà `yarn`, consultez [le site web](https://classic.yarnpkg.com/lang/en/) pour l'installer sur votre système.

Une fois `yarn` installé, ouvrez une ligne de commande (cmd, terminal, bash...) et allez dans le dossier où vous avez décompressé les sources de Karaoke Mugen. Lancez alors la commande :

```sh
yarn setup
```

`yarn` va installer les modules nécessaires au bon fonctionnement de l'application et construire le client React. Cela peut prendre de 1 à 5 minutes selon la rapidité de votre connexion Internet ou de votre machine. Sans accès à Internet, l'installation ne pourra pas continuer.

Vous pouvez répéter les mêmes instructions quand vous mettez à jour votre application, n'oubliez juste pas de transférer le dossier app vers la nouvelle version pour ne pas avoir à recommencer du début.

Une fois que c'est fait, vous pouvez taper `yarn start` pour démarrer, vous pouvez passer à la section [démarrage](getting-started.md) !

## Spécificités selon la plateforme

### Linux

Vous aurez besoin de PostgreSQL d'installé et prêt à l'emploi. [Consultez la section dédiée à l'installation depuis les sources pour savoir comment le configurer](#installer-karaoke-mugen-depuis-les-sources)

### Linux (Ubuntu)

Dans les versions anciennes de Ubuntu (avant 22.04), la version de mpv fournie dans le dépôt Ubuntu officiel est obsolète, **et n'est pas compatible avec Karaoke Mugen**.

Il est donc nécessaire d'installer une autre version. Le [site officiel](https://mpv.io/) fournit plusieurs méthodes d'installation, mais la plus simple pour les distributions basées sur [Ubuntu](https://www.ubuntu.com/) est d'utiliser le PPA :

```sh
sudo add-apt-repository ppa:mc3man/mpv-tests
sudo apt-get update
sudo apt-get install mpv
```

Consultez le [site web de mpv](http://mpv.io) pour plus d'informations.

### MacOS

#### Patch

La version fournie avec macOS de Patch est trop ancienne, vous devrez utiliser [Homebrew](https://brew.sh) pour l'installer.
