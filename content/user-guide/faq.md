+++
title = "Frequently Asked Questions"
weight = 7
+++

Here are a list of frequently asked questions about issues you might have with Karaoke Mugen.

## I lost my admin password !

Don't panic. You can proceed in two ways :

### Online account

* Use the "Reset your password" button.
**Beware, it will not work if you haven't put an email address in your profile**
    * if you haven't filled in your email, [contact us](https://mugen.karaokes.moe/en/contact.html) so we can help you

### Local account

* Use the "Reset my password" button on the connection page and enter the security code. You'll find it in the "Security Code" application menu, or in the logs if you're running a headless version like on a Raspberry Pi.

![password](/images/user/passwordlost.png)

## My song list is empty or incomplete

This might be normal. Either it's the first time you started the app, or you incorrectly modified an option.

If it's the first (or second) time you started the app, it should have downloaded all songs. This can take a bit of time at launch if your computer or your hard drive is slow.

Before modifying anything in the repository list, make sure no update is currently being done.

In the repository list in the system panel, make sure that the `kara.moe`  repository is active and online. If it isn't or if you're not sure, delete the repository and re-add it from the system panel.

## .kara.json and .ass files are in place but still nothing happens

Make sure that you restarted Karaoke Mugen, it should take into account the new files. You can restart a generation by going to the system panel from the home page, in the **System** menu then **Database** and finally by clicking on the button **Regenerate your database**.

![regen](/images/user/regeneration.png)

## I have everything set up, but the video window is black/blue (Windows only)

Your video card does not support OpenGL. There's an option in the config file to switch to Direct3D under Windows.

In the system panel, in the configuration menu, find `mpvVideoOutput` in the `Player` section and set it to `direct3d`.

## How much space is needed for Karaoke Mugen?

The software should take about 500 Mb of disk space, but the karaoke database can take several hundred gigabytes when fully downloaded.

## Can I download only part of the base?

A download manager is available in the System Panel.

![download](/images/user/download.png)

Songs will be downloaded automatically when you add them to a current playlist.
