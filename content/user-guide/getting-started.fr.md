+++
title = "Utilisation"
weight = 2
+++

Une fois Karaoke Mugen [installé](../install), vous devriez pouvoir le lancer depuis votre menu des applications, Menu Démarrer, Dock, ou peu importe comment vous l'appelez.

Karaoke Mugen devrait alors démarrer.

## C'est parti !

Voici ce qui devrait s'afficher (plus ou moins) dans une fenêtre.

![Initialisation](/images/user/Initialisation.png)

Félicitations, Karaoke Mugen est sur le point de démarrer. Vous allez voir de nombreuses étapes passer, cela ne devrait pas prendre trop longtemps (vous pouvez déplier le journal en cliquant sur le bouton "Logs").

Si vous avez effectué des changements dans les bases de karaokés que vous avez installées, Karaoke Mugen le détectera automatiquement et générera la base en conséquence, cela peut être long. Selon si la base se trouve sur un SSD, un HDD, ou sur un point réseau va aussi grandement influer sur la rapidité de la génération.

Lors du premier démarrage, une série de questions vous sera posée pour savoir si vous désirez créer un compte local à votre ordinateur ou un compte en ligne qui sera utilisable sur toutes les apps Karaoke Mugen (si vous allez faire un karaoké chez quelqu'un d'autre, ou si vous vous connectez en ligne sur [kara.moe](https://kara.moe) par exemple).

Suivez les instructions à l'écran et tout devrait bien se passer.

![Screen accueil](/images/user/Screen accueil.png)

Une fois sur l'écran d'acceuil, le logiciel va chercher à mettre à jour ses données et autres avant d'envoyer ses statistiques (s'il y a une connexion à Internet).

Depuis cette page d'accueil, vous pourrez ensuite lancer le tutoriel, l'interface de gestion ou l'interface publique des utilisateurs (ou afficher ce document d'aide en ligne), mais aussi configurer l'application, télécharger des chansons, consulter les nouveautés et aussi le journal d'évènements.

## Premiers pas

Une fenêtre devrait également s'ouvrir dans le coin inférieur droit avec un fond d'écran et des indications :

![Background](/images/user/background.png)

L'adresse indiquée servira à votre public pour se connecter à l'interface de sélection des karaokés.

Si vous êtes connecté à Internet, les utilisateurs peuvent se connecter à votre karaoké pour voir la playlist en cours ou suggérer des chansons. **Ils ne peuvent pas voir le karaoké en lui-même.** Pour ça, nous vous suggérons d'utiliser Twitch ou Discord.

Si vous n'êtes pas connecté à Internet, votre adresse IP de réseau local sera affichée à la place de l'adresse en kara.moe.

Par défaut, le lecteur vidéo qui va jouer vos karaokés est une petite fenêtre pour vous permettre de le tester et de le déplacer (sur un autre écran, par exemple). Vous pouvez paramétrer le plein écran dans les paramètres de Karaoke Mugen.

Ce mode fenêtré est idéal pour effectuer des tests, ou avoir le karaoké qui se joue en arrière-plan pendant que vous travaillez.

## Interfaces

Karaoke Mugen possède plusieurs interfaces selon ce que vous souhaitez faire. Vous pouvez passer d'une interface à l'autre depuis le menu "Aller à" de l'application, ou depuis la page d'acceuil.

### L'interface publique

Celle-ci est destinée à vos invités/votre public. Cette page leur permet de rechercher des chansons, de les ajouter à la [playlist publique](../playlists/#public), de consulter la playlist courante et publique. Pour y accéder, entrez tout simplement l'adresse indiquée par le fond d'écran du lecteur vidéo !

Vous arriverez alors sur l'interface suivante une fois connecté (par un compte invité par exemple) :

![public](/images/user/public.png)

Si vous vous connectez avec votre compte habituel et que vous avez le rang d'opérateur sur cette application Karaoke Mugen (ce qui devrait êter le cas sur la vôtre) vous arriverez d'abord sur l'écran d'acceuil.

#### Restreindre l'interface

Par défaut l'interface publique est ouverte : tout le monde à accès à toutes ses fonctions. Cependant, en fin de karaoké par exemple il peut être intéressant de restreindre voire fermer cette interface pour éviter que les gens n'ajoutent des chansons.

Trois modes sont disponibles :

* Ouvert : les utilisateurs peuvent ajouter des chansons ou en suggérer
* Restreint : les utilisateurs peuvent uniquement voir le karaoké en cours, les informations de ce dernier et la playlist courante
* Fermé : c'est... fermé. Plus rien n'est possible côté public.

### L'interface opérateur

Vous pouvez vous y connecter via la fenêtre de l'application.

Sans passer par la fenêtre de l'application, depuis un autre périphérique que votre ordinateur, vous pouvez vous connecter page d'administration via l'adresse `http://xxx.kara.moe/admin`.

Vous pouvez facilement depuis l'interface opérateur gérer le lecteur (en haut) et vos différentes listes de lecture. Ici, on a la liste de tous les karaokés à gauche, et la liste de lecture courante à droite.

Essayez d'ajouter des karaokés dans la liste de lecture courante et appuyez sur Play !

![admin](/images/user/admin.png)

### Panneau système

Depuis cette interface, vous pourrez :

* Configurer les sessions de karaoké
* Afficher les logs
* Configurer des options avancées
* Créer et supprimer des utilisateurs
* Créer ou éditer des karaokés
* Créer ou éditer des tags (rattachées à des karaokés)
* Mettre à jour la base de karaokés
* Télécharger des karaokés
* ...et plein d'autres choses !

## Utilisateurs

Chaque utilisateur de Karaoke Mugen a besoin d'un compte, qu'il soit en ligne, ou local.

Les comptes en ligne nécessitent que Karaoke Mugen soit connecté à Internet, et permettent aux gens de sauvegarder leurs favoris, avatar et autres informations pour les retrouver sur chaque instance.

Les comptes en ligne se reconnaissent car ils possèdent un @ dans leur nom. Par exemple `axel@kara.moe` : la deuxième partie fonctionne comme une adresse mail et permet d'identifier sur quel serveur Karaoke Mugen se trouve le compte en ligne.

Vous pouvez également reconvertir votre compte en ligne en compte local : cela supprimera toute possibilité de se connecter avec sur une autre instance et supprimera votre compte de Karaoke Mugen Server (le `kara.moe`)

Si vos utilisateurs ne souhaitent pas créer de compte, il suffit de cliquer sur "Continuer comme invité" pour que le logiciel leur attribue un compte invité.

Les invités ne peuvent pas changer leur nom et n'ont pas de liste de favoris. Ils ne peuvent pas non plus "liker" de suggestion.

{{% notice note "Note pour les utilisateurs avancés" %}}
`kara.moe` est mis à disposition des utilisateurs de Karaoke Mugen, mais vous pouvez ouvrir votre propre serveur Karaoke Mugen pour proposer des comptes en ligne et une URL raccourcie de votre choix (tant que vous possédez le nom de domaine). Le code source du serveur ainsi que ses instructions de déploiement sont [disponibles sur GitLab](https://gitlab.com/karaokemugen/karaokemugen-server).
{{% /notice %}}

## Sessions

Les sessions sont des moments de karaoké. Des soirées, des anniversaires, des évènements... Chaque session contient les karaokés joués et demandés durant celles-ci et peuvent vous servir à faire des stats. Une session est créée automatiquement à chaque fois que vous démarrez Karaoke Mugen, vous pouvez selectionner la session en cours depuis l'écran d'acceuil. Vous pouvez également modifier les sessions dans le panneau système.

Une session marquée "privée" ne sera pas envoyée sur Karaoke Mugen Server pour actualiser les statistiques.
