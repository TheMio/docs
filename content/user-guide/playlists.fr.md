+++
title = "Playlists et lecteur"
weight = 3
+++

Karaoke Mugen fonctionne autour de listes de chansons, [l'interface opérateur](../getting-started/#linterface-opérateur) vous montre deux de ces listes pour vous permettre de faire des opérations entre les deux (ajouter une chanson de la gauche vers la droite, ou inversement, par copie ou par déplacement). On vous explique tout.

## Généralités sur les listes

Les karaokés sont affichés ligne à ligne, avec dans l'ordre : leur langue, leur série (ou leur chanteur si il n'y a pas de série), leur type (Opening, Ending, MV, etc.) et leur nom. Les lignes avec une icône <i class="fas fa-history"></i> orange indiquent les karaokés qui sont passés à l'écran il y a moins d'une heure (ce délai est configurable), pratique pour éviter la redondance. La ligne en bleu indique un karaoké qui est positionné pour la lecture (celui qui sera joué si la playlist est [courante](#liste-de-lecture-courante)).

{{% notice info "A propos des noms dans différentes langues" %}}
Vous pouvez définir vos préférences linguistiques pour le nom des séries (Romaji, Japonais, Anglais, etc.) dans les paramètres de votre profil, accessibles dans le menu K en haut à droite.
{{% /notice %}}

## Bibliothèque <i class="fas fa-book"></i>

La bibliothèque montre toutes les karaokés que le logiciel connaît, ce sont celles que le logiciel a récupéré des dépôts configurés ou que vous avez [créées](../../contrib-guide/create/). Ils sont triés alphabétiquement par la série ou par le chanteur si il n'y a pas de série. Vous pouvez changer de tri en cliquant sur le bouton filtre <i class="fas fa-filter"></i> à côté du sélecteur de liste.

## Listes de lecture <i class="fas fa-list-ol"></i>

Vous pouvez créer autant de listes de lecture que vous voulez. Ces listes peuvent être utiles pour préparer à l'avance des soirées, ou bien pour écouter des chansons selon vos humeurs. Une liste de lecture peut avoir autant de karaokés que vous le souhaitez, mais un karaoké ne peut pas exister deux fois dans la même liste de lecture, vous pouvez lever cette restriction dans les paramètres.

Vous aurez forcément une liste de lecture **courante**, une liste de lecture **publique**, une liste **noire** et une liste **blanche** qui rempliront des rôles bien particuliers. Voici un petit schéma pour expliquer leurs rôles.

![](/images/user/playlists_schema.png)

### Liste de lecture publique <i class="fas fa-globe"></i>

La liste de lecture publique est la liste qui accueillera tous les sons que les utilisateurs ajoutent via [l'interface publique](../getting-started/#linterface-publique). Il ne peut y avoir qu'une seule liste de lecture publique. Pour désigner une liste de lecture comme publique, il suffit de cliquer sur Éditer la liste dans le menu clé à molette <i class="fas fa-wrench"></i> et de cocher la case Publique (cette liste remplacera donc l'ancienne liste publique).

### Liste de lecture courante <i class="fas fa-play-circle"></i>

La liste de lecture courante est la liste qui sera utilisée par le [lecteur vidéo](#lecteur-video) et qui sera contrôlée via les boutons Jouer/pause, suivant, précédent, dans l'en-tête de [l'interface opérateur](../getting-started/#linterface-operateur). La ligne affichée en bleu est donc le karaoké en cours de lecture. Il ne peut y avoir qu'une seule liste de lecture courante. Pour désigner une liste de lecture comme courante, il suffit de cliquer sur Éditer la liste dans le menu :fa-wrench: et de cocher la case Courante (cette liste remplacera donc l'ancienne liste cournate).

### Liste à la fois publique et courante <i class="fas fa-play-circle"></i> <i class="fas fa-globe"></i>

Rien ne vous empêche d'avoir une liste qui remplit les deux rôles : publique et courante, elle sera donc jouée par le lecteur, mais recevra également en temps réel les suggestions de votre public. C'est utile si vous faites confiance à vos invités et que vous ne voulez pas modérer ce qu'ils veulent chanter.

Avoir deux listes différentes, une publique et une courante vous permet de filtrer les suggestions de vos invités avant de les envoyer au lecteur !

### Liste noire

Les chansons dans la liste noire n'apparaîtront plus dans les résultats de recherche visibles du public. Les opérateurs par contre pourront toujours les voir. Cependant, si vous ajoutez des karaokés blacklistés à une playlist, le public pourra **voir ces chansons**.

En général on fera une liste noire "intelligente" obéissant à des critères, mais on peut aussi vouloir utiliser une liste noire classique où on ajoute à la main les chansons qu'on ne veut pas.

Si vous souhaitez ajouter des exceptions, vous pouvez utiliser la [liste blanche](#liste-blanche).

### Liste blanche <i class="fas fa-check-circle"></i>

Dans cette liste, vous pouvez ajouter toutes les exceptions aux critères que vous avez définis précédemment. Par exemple, si vous voulez bannir tous les karaokés de *mechas*, vous pouvez vouloir garder celui de King Gainer, parce qu'il est cool, en ajoutant celui-ci à la liste blanche.

## Listes intelligentes

Karaoke Mugen propose un système de liste intelligente fonctionnant par critères, ces listes peuvent endosser les rôles expliqués ci-dessus.

Les karaokés répondant à ces critères apparaîtront dans la liste automatiquement. Si de nouvelles chansons sont ajoutées à la bibliothèque elles seront aussi ajoutées automatiquement.

Une liste intelligente ne peut pas être convertie en liste normale et inversement.

### Critères de liste intelligente

Vous pouvez passer de l'affichage liste à critères et inversement sur une liste intelligente.

Les critères sont par exemple :

- **Titre contenant** : Karaokés dont le titre contient le mot sélectionné (exemple "Gundam").
- **Plus long que/plus court que** : Tous les karaokés plus longs ou plus courts que la valeur indiquée, en secondes. Pratique pour bannir les AMVs et autres chansons longues.
- **Tags** : Tag précis
- **Chanteur**
- **Type** : Type de chanson (Opening, Ending, Clip, AMV, etc.).
- **Créateur** : Entité créatrice de l'œuvre.
- **Auteur du Kara** : Auteur du karaoké (celui qui a écrit les fichiers permettant de chanter).
- **Langue**
- **Divers** : Tags divers
- **Compositeur**
- etc.

Prenons le cas d'une liste noire : celle-ci peut être intelligente et reposer sur des critères. Quelques idées de choses à bannir : les karaokés plus longs que 4 minutes, ceux ayant le tag Spoiler ou Pour Adultes, etc.

Pour revenir sur la liste intelligente, celle-ci possède également d'autres paramètres :

- Limiter la liste à X chansons ou X durée en triant celle-ci. Pratique si vous voulez vous faire une playlist automatique "Top 100" par exemple.
- Choix d'union ou d'intersection (OU/ET) :
  - Si vous selectionnez "OU", chaque critère de la liste ajoutera ses propres chansons à celle-ci. Par exemple "Tag: Pour Adultes" OU "Série: Gundam" selectionnera **tous les karaokés pour adultes + ceux de Gundam.**
  - Si vous selectionnez "ET", pour qu'un karaoké soit ajouter à la liste il doit **satisfaire tous les critères**. Par exemple "Série: Pokémon" ET "Durée: moins de 2 minutes" selectionnera tous les karaokés de Pokémon qui font moins de 2 minutes. Avec "ET" et des critères trop différents il est possible qu'aucun karaoké ne s'affiche dans la liste (Par exemple "Durée: supérieure à 3 minutes" et "Année: 1982" car aucune chanson ne satisfait ces critères simultanément)

Dans le cas d'une liste intelligente noire (servant donc à bannir des chansons) vous voudrez probablement utiliser "OU".

Pour une liste intelligente servant de playlist, "ET" sera plus utile.

## Listes de favoris <i class="fas fa-star"></i>

Chaque utilisateur a sa liste de favoris personnelle dans laquelle il peut retrouver rapidement les pistes qu'il utilise le plus souvent. Si vous avez un compte en ligne, cette liste est consultable sur le [site Internet de la base](https://kara.moe), en vous connectant à votre compte.

Pour ajouter un titre à sa liste de favoris, il faut cliquer sur la clé à molette <i class="fas fa-wrench"></i> à gauche d'un karaoké depuis la vue opérateur, ou depuis la vue publique d'abord sélectionner un karaoké pour lui faire afficher les détails et enfin cliquer sur le bouton <i class="fas fa-star"></i> Ajouter aux favoris.

Les utilisateurs peuvent utiliser leur liste de favoris pour suggérer rapidement des titres à l'opérateur.

## Listes AutoMix <i class="fas fa-bolt"></i>

Un AutoMix est une playlist générée automatiquement à partir des favoris des utilisateurs sélectionnés au moment de la création, pour une durée donnée. Pour créer un AutoMix, vous pouvez cliquer sur le menu roue dentée <i class="fas fa-cog"></i> à côté du sélecteur de liste.

Les listes générées par AutoMix peuvent tout à fait être définies comme liste [courante](#liste-de-lecture-courante) ou [publique](#liste-de-lecture-publique) par la suite.

## Likes

Lors de la consultation de la liste des chansons, les utilisateurs peuvent *like* un karaoké demandé afin de le faire remonter pour que l'opérateur voie quels sont les karaokés les plus populaires de sa liste.

Comme chaque utilisateur a un nombre de demandes d'ajout limité lors d'une session, si sa demande (l'opening 1 de Naruto par exemple) est suffisamment *likée*, elle ne compte plus comme demande et l'utilisateur peut ainsi demander d'autres karaokés supplémentaires ! Ce mécanisme de libération de karaokés est [configurable à loisir](../config/#libération) (combien de *likes* sont nécessaires, quel pourcentage de la population connectée, etc.).

Les opérateurs peuvent libérer des chansons manuellement via le menu clé à molette <i class="fas fa-wrench"></i> de chaque karaoké.

## Lecteur vidéo

Le lecteur vidéo utilisé est mpv, que vous pouvez trouver sur son site web, [mpv.io](http://mpv.io).

{{% notice tip "Raccourcis claviers du lecteur vidéo" %}}
Ce lecteur possède quelques raccourcis clavier utiles :

* `ESC` : Quitter le plein écran.
* `Q` : Quitter le lecteur (Le lecteur sera relancé automatiquement dès que vous relancez une chanson).
* `Flèche gauche/flèche droite` : Avancer/reculer un peu (10s) dans la vidéo.
* `Flèche haut/flèche bas` : Avancer/reculer beaucoup (60s) dans la vidéo.
* `F` : Basculer entre le mode plein-écran et fenêtré.
* `Molette de souris haut/bas` : Changer le volume (en laissant votre curseur dans la fenêtre du lecteur).

Ces touches ne fonctionneront que si vous avez le "focus" sur le lecteur vidéo. Cliquez dans la fenêtre pour obtenir le focus.
{{% /notice %}}

Vous pouvez également déplacer la fenêtre à votre guise sur votre écran. Par défaut, elle est toujours au-dessus des autres, mais cela peut être configuré.
