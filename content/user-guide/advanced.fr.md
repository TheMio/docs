+++
title = "Utilisation avancée"
weight = 6
+++

Cette section est dédiée au charabia technique, aux définitions des concepts évoqués un peu partout. Nous y expliquons le fonctionnement de la structure de dossier Karaoke Mugen ou le format du fichier de configuration par exemple.

## Dossiers

Karaoke Mugen stocke les données utilisateur :

- dans le dossier `KaraokeMugen` à la racine de votre dossier utilisateur (`C:\Users\<nom d'utilisateur>` sur Windows, `/Users/<nom d'utilisateur>` sur Mac, `/home/<nom d'utilisateur>` sur Linux) si vous avez utilisé l'installateur Karaoke Mugen.
- Dans le dossier `app` à la racine de l'application, si vous utilisez un package portable.

C'est ici que vous trouverez le [fichier de configuration](#configuration), mais aussi les karaokés téléchargés, les [médias de liste de lecture (Intros, Outros, Jingles, Sponsors, etc.)](#medias-de-liste-de-lecture), les [fonds d'écran](#fonds-decran) etc.

## Base de données

Karaoke Mugen stocke ses données dans une base de données PostgreSQL se trouvant dans `db/postgres`

La version binaire de Karaoke Mugen inclut un serveur PostgreSQL lancé au démarrage du logiciel donc vous n'avez à vous soucier de rien. Vous pouvez néanmoins utiliser votre propre serveur PostgreSQL si vous le souhaitez en suivant les instructions dans [la section configuration](#configuration).

Lors d'une mise à jour du logiciel, la base de données est mise à jour en conséquence de façon transparente pour vous.

## Ligne de commande

Voici les options de ligne de commande pour l'application **Karaoke Mugen**.

* `--cli` : Démarre Karaoke Mugen sans ouvrir de fenêtre Electron.
* `--config fichier` : Spécifie un fichier de configuration à lire à la place du fichier par défaut `config.yml`
* `--debug` : Affiche les messages de débogage dans la console (normalement, ceux-ci sont uniquement lisibles dans le fichier de log).
* `--dumpDB` : Crée un backup de la base de données dans le fichier `karaokemugen.sql` et ferme l'application
* `--forceAdminPassword <password>` : Change le mot de passe du compte admin. Remplacez `<password>` par le mot de passe souhaité.
* `--generate` : Génère une base de données puis ferme l'application. Cela a pour effet d'écraser l'ancienne base.
* `--help` : Affiche le message d'aide.
* `--kill` : Arrête une autre app KM déjà lancée.
* `--noAutoTest` : Utilisé en conjonction avec `--test`, ne lance pas automatiquement le test à la fin de l'initialisation
* `--noBaseCheck` : Ne vérifie pas au lancement les fichiers de la base de karaokés (afin d'aller plus vite, si vous êtes sûrs que rien n'a changé)
* `--noBrowser` : N'essaye pas d'ouvrir de navigateur au démarrage (uniquement démarrage sans Electron).
* `--noMedia` : À utiliser avec `--generate` ou `--validate` pour faire abstraction des fichiers médias qui n'existent pas (option pour les développeurs).
* `--noPlayer` : Ne démarre pas le lecteur vidéo de KM pour gagner un peu de temps au démarrage. Le lecteur sera démarré automatiquement au besoin.
* `--profiling` : Affiche les infos de profiling dans la console
* `--reset` : Réinitialise toutes les données utilisateur. **Attention cela supprimera tous les utilisateurs locaux, les playlists crées, etc.**
* `--restoreDB` : Restaure un backup de la base de données depuis le fichier `karaokemugen.sql` et ferme l'application
* `--sql` : Affiche les requêtes SQL en mode débogage.
* `--strict` : À utiliser avec `--generate` ou `--validate` : échoue si un .kara doit être modifié par la procédure de génération de la base de données. De moins en moins utile de par l'apparition du formulaire de création de karaokés.
* `--test` : Tourne en mode test, utilisé pour les tests unitaires qu'on peut réaliser via la commande `yarn test` (développeurs uniquement).
* `--updateBase` : Met à jour la base de karaokés et déclenche une nouvelle génération de base de données.
* `--updateMediasAll` : Ne met à jour que les médias, pas les .JSON, ni les .ASS.
* `--validate` : Teste et valide les karaokés trouvés dans les dossiers de données (`app/data` par défaut). Cela est déjà fait via la génération. La différence avec `--generate` c'est qu'aucune base de données n'est touchée dans ce cas.
* `--version` : Affiche la version actuelle.

## Configuration

Cette section décrit le fichier de configuration du logiciel. Vous n'avez normalement pas à y toucher sauf pour une utilisation avancée. Vous pouvez à tout moment éditer les principaux paramètres de l'application via l'interface opérateur. Plus d'options avancées sont disponibles dans le panneau système.

Karaoke Mugen utilise un fichier de configuration nommé `config.yml` qui se situe dans votre [dossier de données](#dossiers).

Si ce fichier n'existe pas, il utilise des paramètres par défaut. Il y a un exemple de fichier appelé `config.sample.yml` dans les sources.

Lorsque vous modifiez un paramètre via l'interface opérateur, un fichier `config.yml` est écrit (ou modifié s'il existe déjà) avec les nouveaux paramètres, mais tout n'est pas configurable via l'interface opérateur.

Le panneau système vous permettra par contre de modifier absolument tous les paramètres dans le menu "Configuration avancée". En général, ce n'est pas nécessaire, faites attention à ce que vous faites dans ce menu.

### App

Section dédiée aux paramètres généraux.

* `FirstRun` : Passé à `false` par Karaoke Mugen pour indiquer que celui-ci s'est déjà exécuté et que l'utilisateur ne déclenchera pas le tutoriel de nouveau.
* `JwtSecret` : Identifiant secret servant à saler les jetons de connexion.
* `QuickStart` : Booléen pour définir si l'app doit faire un démarrage rapide ou non, `false` par défaut.
	* Cela implique que l'application ne vérifie pas votre base de données avant le démarrage, ce qui peut mener à des problèmes de cohérence. N'hésitez pas à regénérer votre base de données en utilisant le bouton dans le panneau système si vous avez des problèmes.

### Online

Paramètres de configuration des fonctionnalités en ligne.

* `AllowDownloads` : Booléen permettant d'activer/désactiver les téléchargements de chansons. Si c'est désactivé, l'application ne téléchargera aucun média et les streamera à la place.
* `ErrorTracking` : Booléen permettant d'activer/désactiver l'envoi des rapports d'erreur de l'application sur notre compte [sentry.io](https://sentry.io)
* `FetchPopularSongs` : Lorsqu'il est à `true`, la section des chansons populaires dans l'interface publique sera issue des statistiques disponibles dans les dépôts distants configurés.
* `Host` : Nom d'hôte auquel se connecter pour les fonctionnalités en ligne. Cela remplacera aussi l'URL affichée sur l'écran du lecteur. Fixé sur `kara.moe` par défaut.
* `MediasHost` : Chemin HTTP sur lequel les vidéos se trouvent. Si jamais elles ne sont pas disponibles dans un des dossiers `Medias`, Karaoke Mugen essayera de les lire via cette adresse avant d'essayer l'URL du dépôt concerné. Pratique pour établir un serveur de cache.
* `Port` : Définit le port du serveur KM Server auquel l'application doit se connecter. Utilisé surtout à des fin de débuggage. `undefined` par défaut.
* `Remote` : Définit si l'instance est disponible à distance via le système de proxy proposé par votre instance en ligne (définie dans `Host`). Cela permet à vous de ne pas avoir à configurer votre réseau pour exposer votre instance aux utilisateurs extérieurs à votre réseau.
* `Stats` : Envoi ou non des statistiques d'utilisation de Karaoke Mugen à Karaoke Mugen Server.
     - `undefined` (non-existant) : Pose la question à l'utilisateur au démarrage.
	 - `false` : Aucun envoi ne sera fait.
	 - `true` : L'envoi des stats est actif.
* `Users` : Permet aux utilisateurs de créer ou de se connecter à un compte en ligne.
	  - `false` : Les utilisateurs ne pourront utiliser que des comptes locaux. Si un compte en ligne a déjà été créé sur l'instance, le nom d'utilisateur deviendra `user@truc.com`. Par exemple `axel@kara.moe`
	  - `true` : Les utilisateurs peuvent se connecter à leur compte en ligne ou en créer.

#### Discord

* `DisplayActivity` : Booléen. Afficher ou non l'activité de Karaoke Mugen sur votre profil Discord. Affiche si vous êtes en train de chanter ou non et sur quelle chanson.

#### Updates

Définit ce qui va être mis à jour automatiquement :

Toutes les valeurs sont à `true` par défaut.

* `App` : Booléen, vérifie si une nouvelle mise à jour est disponible ou non.

#### Medias

* `Intros` : Booléen, définit si les intros sont mises à jour depuis le Gitlab de Shelter.
* `Jingles` : Booléen, même chose avec les jingles
* `Encores` : Pareil.
* `Outros` : Pareil.
* `Sponsors` : Pareil.

### Frontend

Paramètres liés à l'interface web.

* `Mode` : Mode d'ouverture de l'interface web.
	  - `0` : Interface fermée à tout utilisateur (sauf l'admin).
	  - `1` : Interface limitée pour les utilisateurs. Affichage de la playlist en cours et du karaoké en cours uniquement.
	  - `2` : Interface ouverte totalement (par défaut).
* `Port` : Port réseau à utiliser pour l'interface web de Karaoke Mugen.
* `SeriesLanguageMode` : Mode d'affichage des séries des chansons.
    - `0` : La série porte son titre d'origine (donc en japonais pour un anime).
	- `2` : La série porte son titre selon la langue du système sur lequel Karaoke Mugen tourne (ou se rabat sur l'anglais, etc.).
	- `3` : La série porte son titre selon la langue du navigateur de l'utilisateur (...).
* `ShowAvatarsOnPlaylist` : Booléen. Affichage des avatars ou non sur les listes de lecture dans l'interface.

#### Permissions

* `AllowNicknameChange` :
	- `false` : Les utilisateurs ne peuvent pas changer de pseudo.
	- `true` : Les utilisateurs peuvent changer de pseudo (par défaut).

### GUI

* `OpenInElectron` :
    - `false` : Tous les liens cliqués dans la fenêtre Electron de l'app s'ouvriront via le navigateur.
	- `true` : Tous les liens cliqués dans la fenêtre Electron resteront dans cette fenêtre.

#### ChibiPlayer

* `Enabled` : Booléen
* `AlwaysOnTop` : Booléen
* `PositionX` : Nombre
* `PositionY` : Nombre

#### ChibiPlaylist

* `Enabled` : Booléen
* `PositionX` : Nombre
* `PositionY` : Nombre

### Karaoke

Réglages spécifiques à votre session karaoké :

* `AutoBalance` : Quand quelqu'un ajoute une chanson à la liste de lecture courante, Karaoke Mugen essayera d'appliquer la méthode de tri "équilibrée" à la playlist.
* `Autoplay` : Si aucun karaoké n'est en cours de lecture, la playlist démarrera toute seule dès qu'un karaoké y sera ajouté.
    - `false` : Le démarrage de la lecture est manuel (par défaut).
	- `true` : La lecture démarre toute seule si un karaoké est ajouté.
* `ClassicMode` : Si activé, la lecture s'arrêtera après chaque chanson et ça sera à l'administrateur ou à celui qui a demandé la chanson de lancer la suivante via son appareil.
    - `false` : Mode classique désactivé.
	- `true` : Mode classique activé.
* `MinutesBeforeEndOfSessionWarning` : Quand une fin de session est programmée, un warning s'affichera pour l'opérateur X minutes avant la fin de la session.
* `Private` :
	- `false` : Passe en mode karaoké public, les ajouts des invités vont dans la liste de lecture publique.
	- `true` : Passe en mode karaoké privé, les ajouts des invités vont directement dans la liste de lecture courante (par défaut).
* `Repeat` : Répétition auto de la liste de lecture courante.
    - `false` : La lecture s'arrête quand la playlist arrive à la fin (par défaut).
    - `true` : La lecture continue en reprenant la playlist depuis le début.
* `SmartInsert` :
	- `false` : L'insertion intelligente est désactivée.
	- `true` : Les karaokés seront intelligemment insérés dans la playlist courante, en favorisant les utilisateurs qui n'ont pas beaucoup ajouté de karaokés, par exemple.

#### Collections

Ici sont listés les collections (sous forme de Tag ID) ainsi que leur état d'activation.

Exemple:

```yaml
Collections:
  c7db86a0-ff64-4044-9be4-66dd1ef1d1c1: true # Otaku/Geek
  efe171c0-e8a1-4d03-98c0-60ecf741ad52: true # World
  2fa2fe3f-bb56-45ee-aa38-eae60e76f224: true # Shitpost
```

#### ConnectionInfo

* `Enabled` : Karaoke Mugen doit-il afficher l'URL de connexion lors des pauses ou jingles ?
    - `false` : Aucune URL ne s'affichera en bas de l'écran.
    - `true` : Les infos de connexion sont affichées (par défaut).
* `Host` : Si vide, l'adresse IP est détectée automatiquement, mais cette détection automatique peut parfois être fausse : cela vous permet d'indiquer vous-même l'adresse que vos invités utiliseront pour se connecter.
* `Message` : Message à afficher en plus de l'adresse de connexion. Exemples :
    - 1 euro la chanson !
    - Réseau WiFi : Jonetsu

#### Poll

* `Choices` : Nombre de chansons à suggérer dans les votes.
* `Enabled` : Votes publics.
    - `false` : Les votes publics sont désactivés.
    - `true` : Les votes publics sont activés.
* `Timeout` : Durée d'un sondage. Il sera arrêté dix secondes avant la fin d'une chanson.

#### Quota

* `FreeAutoTime` : Temps en minutes avant qu'une chanson ne soit automatiquement libérée (60 minutes par défaut).
* `FreeUpvotes` :
	- `false` : Les "likes" ne permettent pas de donner un ajout gratuit en liste courante.
    - `true` : Les utilisateurs dont un karaoké est énormément "liké" obtiennent un ajout gratuit dans la liste courante.
* `FreeUpvotesRequiredMin` : Nombre de likes minimum requis pour qu'il y ait attribution d'un ajout gratuit.
* `FreeUpvotesRequiredPercent` : Pourcentage (0-100) de likes requis pour obtenir un ajout gratuit. Il s'agit du pourcentage de gens connectés au moment du "like".
* `Songs` : Nombre de chansons qu'une personne peut avoir dans une même liste de lecture.
* `Time` : Durée totale en secondes de toutes les pistes qu'une personne peut avoir dans une même liste de lecture.
* `Type` : Type de quota à utiliser pour contrôler la capacité des utilisateurs à ajouter des chansons.
    - `0` : Aucun quota.
	- `1` : Nombre de chansons.
	- `2` : Durée.

#### StreamerMode

Le mode Streamer permet d'afficher les résultats des votes des participants lors de la sélection d'une chanson, mais aussi de faire interagir le public de Twitch. Il ajoute également une pause configurable entre les chansons.

* `Enabled` : `true` ou `false` (par défaut) selon si on veut activer ce mode.
* `PauseDuration` : Durée de la pause en secondes.

##### Twitch

* `Enabled` : `true` ou `false` (par défaut) selon si on veut activer le lien vers Twitch.
* `OAuth` : Votre jeton OAuth Twitch.
* `Channel` : Canal twitch à rejoindre pour le bot.

### Player

* `Background` : Nom du fichier image présent dans le dossier `app/backgrounds`.
    - Si aucun n'est spécifié, Karaoke Mugen choisira un fond d'écran aléatoire à chaque pause selon ce qu'il trouvera dans le dossier.
	- Si aucun fond d'écran n'est détecté dans le dossier, celui par défaut sera utilisé.
* `Borders`:
    - `false` : Le lecteur principal n'affiche aucune bordure.
	- `true` : Le lecteur principal affiche les bordures et décorations de fenêtre, vous permettant de redimensionner.
* `ExtraCommandLine` : Chaîne de caractères d'options de ligne de commande à ajouter à mpv lors de son lancement.
* `Fullscreen` : Ce paramètre n'est pas pris en compte si `PIP.Enabled` est à `true`.
	- `false` : Le lecteur est en mode fenêtré (par défaut).
	- `true` : Le lecteur est en plein écran.
* `HardwareDecoding` :
    - `no` : Décodage matériel désactivé
	- `yes` : Forcer le décodage matériel. Attention cela peut avoir des effets néfastes voire occasionner des plantages selon votre matériel, drivers, et la vidéo utilisée.
	- `auto-safe` : Activer uniquement pour les combinaisons connues pour fonctionner de façon sûre. Consultez la documentation de mpv pour plus d'informations.
* `LiveComments`:
    - `false` : Désactive les commentaires live à l'écran
	- `true` : (par défaut) Active les commentaires live à l'écran. Note que cela nécessite l'utilisation de Twitch.
* `Monitor` :
    - `false` : Le moniteur n'est pas affiché (par défaut).
	- `true` : Le moniteur est affiché. Le moniteur est une deuxième fenêtre du lecteur vidéo qui est synchronisée avec la première. Elle permet à l'opérateur de karaokés d'avoir une vue sur ce qu'il se passe sur l'écran principal.
* `mpvVideoOutput` : Choix du driver vidéo pour mpv. Si sous Windows vous voyez une fenêtre bleue en lançant Karaoke Mugen, indiquez la valeur `direct3d`, sinon laissez à vide pour autodétection.
* `NoBar` :
	- `false` : La barre de progression de la vidéo est affichée lorsqu'on se déplace dans la vidéo.
	- `true` : La barre de progression de la vidéo n'est pas affichée lorsqu'on se déplace dans la vidéo (par défaut).
* `NoHud` :
	- `false` : Les informations du lecteur vidéo mpv sont affichées à l'écran.
	- `true` : Les informations du fichier par mpv ne sont pas affichées à l'écran (par défaut).
* `ProgressBarDock` : (booléen) Affiche la progression du karaoké ou de la génération dans la barre des tâches Windows ou le dock macOS.
* `Screen` : numéro de votre écran.
	- `0` : premier écran (écran principal) (par défaut).
	- `1` : deuxième écran.
	- `2` : troisième écran.
	- ...
	- `9` : Laisser le lecteur décider.
* `StayOnTop` :
	- `false` : Le lecteur ne reste pas au-dessus des fenêtres.
	- `true` : Le lecteur reste toujours au-dessus des autres fenêtres (par défaut).
* `Volume` : de 0 à 100, volume de départ de mpv.

#### Display

* `Avatar` : Afficher l'avatar du demandeur d'un karaoké en bas à droite de l'écran avec les infos de la chanson en cours.
    - `false` : Ne pas afficher d'avatars.
	- `true` : Afficher les avatars.
* `Nickname` :
	- `false` : Le lecteur n'affiche pas qui a demandé la chanson.
	- `true` : Le lecteur affiche qui a demandé la chanson (par défaut).
* `RandomQuotes` :
    - `false` : Les citations aléatoires amusantes de Karaoke Mugen ne s'afficheront pas sous la bannière
	- `true` : (par défaut) Les citations seront affichées.
* `SongInfo` :
    - `false` : Pas d'info sur la chanson d'affichée en début et en fin de lecture. Si c'est désactivé, `Avatar` et `Nickname` ne seront pas affichés.
	- `true` : (par défaut) Les informations sont affichées.
* `SongInfoLanguage` : Un code [ISO639-2B language name](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-2). En plus des codes standard, KM supporte aussi `qjr` pour Japonais - Romaji.

#### PIP

* `Enabled` :
	- `false` : Désactive le mode Picture-in-Picture.
	- `true` : Active le mode Picture-in-Picture (par défaut).
* `PositionX` : Définit la position horizontale du lecteur :
	- `Left` : À gauche de l'écran.
	- `Center` : Au milieu de l'écran.
	- `Right` : À droite de l'écran.
* `PositionY` : Définit la position verticale du lecteur :
	- `Top` : En haut de l'écran.
	- `Center` : Au milieu de l'écran.
	- `Bottom` : En bas de l'écran.
* `Size` : si le mode PIP est activé, définit la taille qu'occupe la vidéo, en pourcentage de la taille de l'écran.

### Playlist

* `AllowDuplicates` : Une chanson peut-elle être ajoutée plusieurs fois (listes non publiques uniquement) ?
* `EndOfPlaylistAction` :
	- `none` : Ne rien faire, le karaoké s'arrête.
	- `random` : Jouer des chansons aléatoirement depuis la bibliothèque
	- `repeat` : Revenir au début de la playlist
* `MaxDejaVuTime` : Une chanson est marquée comme "déjà vue" si elle est passée il y a moins de xx minutes (par défaut 60).
* `RandomSongsAfterEndMessage` : Si activé, affichera un message invitant les gens à ajouter des chansons à la liste de lecture courante lorsque l'action de fin de playlist est `random`.

#### Medias

Pour chaque élément, il y a trois paramètres :

* `Enabled` : Booléen qui active ou désactive l'utilisation de ce type de média
* `File` : (Sauf pour Jingles et Sponsors) Nom d'un fichier à utiliser pour éviter d'en choisir un aléatoirement.
* `Interval` : (Jingles et Sponsors uniquement) Nombre de chansons avant de les jouer.
* `Message` : Un message optionnel à afficher durant la lecture du média.

Pour les jingles et sponsors uniquement :

* `Interval` : Nombre de chansons entre deux jingles/sponsors

Les médias sont `Jingles`, `Sponsors`, `Intros`, `Outros` et `Encores`. Exemple :

```YAML
    Intros:
      Enabled: true
      File: PS1-Nanami.mp4
    Outros:
      Enabled: false
      File: Byebye.mp4
    Encores:
      Enabled: false
      File: encoru.mp4
```

#### MysterySongs

* `AddedSongVisibilityAdmin` :
    - `false` : Les chansons ajoutées par l'administrateur sont "mystérieuses".
	- `true` : Les chansons ajoutées par l'administrateur sont totalement visibles (par défaut).
* `AddedSongVisibilityPublic` :
    - `false` : Les chansons ajoutées par le public sont "mystérieuses".
	- `true` : Les chansons ajoutées par le public sont totalement visibles (par défaut).
* `Hide` :
    - `false` : Les chansons mystères sont visibles (mais leur contenu reste mystérieux) (par défaut).
	- `true` : Les chansons mystères sont cachées, elles n'apparaissent pas du tout dans la liste de lecture pour le public.
* `Labels` : Définit le texte qui apparaitra dasn les playlists à la place du titre des karaokés "mystères". Fixé à `???` par défaut.

### System

#### Binaries

Chemins vers les binaires. Chaque binaire contient trois paramètres de chaîne : `Windows`, `OSX` et `Linux`. Les binaires sont `Player`, `ffmpeg`, `Postgres` et `Patch`. Pour `Postgres` il s'agit du dossier binaire de la distribution postgres. Pour les autres, un exécutable est attendu.

Exemple :

```YAML
System:
  Binaries:
    Player:
      Linux: /usr/bin/mpv
      OSX: app/bin/mpv.app/Contents/MacOS/mpv
      Windows: app/bin/mpv.exe
```

#### Database

Indique les instructions d'accès à votre base de données, vous pouvez généralement tout laisser par défaut.
Si vous utilisez votre propre serveur PostgreSQL, spécifiez `bundledPostgresBinary` à `false` et modifiez ce qui devrait l'être (le mot de passe par exemple si vous l'avez choisi.). Dans ce cas `superuser` et `superuserPassword` sont inutilisés.

```YAML
System:
  Database:
    bundledPostgresBinary: false
    database: 'karaokemugen_app'
    host: 'localhost'
    password: 'musubi'
    port: 5432
    schema: 'public'
    superuser: 'postgres'
    superuserPassword: null
    user: 'karaokemugen_app'
```

Tout est assez explicite. Modifiez les paramètres selon ce dont vous avez besoin.

#### MediaPath

* `Intros` : Chemins où chercher les intros vidéos. `intros` par défaut.
* `Sponsors` : Chemins où chercher les sponsors. `sponsors` par défaut.
* `Outros` : Chemins où chercher les outros. `outros` par défaut.
* `Encores` : Chemins où chercher les sponsors. `encores` par défaut.
* `Jingles` : Chemins où chercher les jingles vidéo. `jingles` par défaut.

#### Repositories

Tous les dépôts de votre installation sont listés dans cette section.

##### Example repository `kara.moe`

* `Enabled` :
    - `true` : Le dépôt sera utilisé pour la génération et les mises à jour en ligne
	- `false` : Le dépôt NE sera PAS utilisé pour la génération et les mises à jour en ligne. Ses chansons n'apparaîtront pas dans votre base de données.
* `Name` : Nom du dépôt. Il faut qu'il soit en accord avec ce qui est présent dans les fichiers `.kara.json` et`.tag.json`. Pour les dépôts en ligne il faut que le nom soit un nom de domaine qui répond.
* `Online` :
	- `true` : Le dépôt est un dépôt en ligne.
	- `false` : Le dépôt ne contient que des fichiers locaux.
* `BaseDir` : Dossier de base où va être cloné le dépôt s’il est en ligne, dossier où se trouve vos dossiers `karaokes`, `lyrics`, `tags`.
* `MaintainerMode` : Mode "Mainteneur du dépôt", indique que le dépôt est géré à travers de moyen externes, ce qui empêchera Karaoke Mugen de mettre à jour automatiquement le dépôt.
* `AutoMediaDownloads` : Définit le comportement vis-à-vis des téléchargements de médias
  - `all` : Dès qu'il y a un média à télécharger depuis le dépôt distant, il sera téléchargé, de ce fait, toute la base fonctionnera sans Internet.
  - `updateOnly` : (option par défaut) Ne téléchargera aucune vidéo mais se chargera de mettre à jour les vidéos déjà téléchargée.
  - `none` : Aucune vidéo ne sera téléchargée automatiquement, tout sera fait manuellement.
* `Path` : Chemins pour chaque type de données :
  - `Medias` : Chemin vers les fichiers médias.

#### Path

Les chemins qui peuvent prendre plusieurs dossiers sont `Backgrounds`, `Jingles`, `Karas`, `Lyrics`, `Medias` et `Tags`. Vous devez séparer les différents dossiers par un retour à la ligne. Tous ces chemins sont relatifs au [dossier de données](#dossiers) de Karaoke Mugen (vous pouvez aussi spécifier un chemin absolu).

* `Avatars` : Chemin de stockage des avatars des utilisateurs.
* `Backgrounds` : Chemins où chercher les fonds d'écran. `backgrounds` par défaut.
* `BundledBackgrounds` : Dossier où KM va stocker ses fonds d'écrans par défaut. Nettoyé à chaque démarrage
* `DB` : Chemin vers les fichiers des bases de données.
* `Import` : Chemin de la base pour l'importation de karaokés.
* `Previews` : Chemin de stockage les miniatures de vidéos. Comptez 500 Ko par karaoké.
* `Temp` : Chemin vers les fichiers temporaires.
* `SessionExports` : Chemin où stocker les exports de session au format csv.
* `StreamFiles` : Chemin où stocker les fichiers texte pour le streaming

## Personnalisation

Outre les quelques options de personnalisation qu'on trouve dans le fichier de configuration, vous pouvez personnaliser Karaoke Mugen un peu plus en changeant quelques médias par défaut.

### Fonds d'écran

Par défaut, un fond d'écran embarqué par Karaoke Mugen est affiché. Vous pouvez néanmoins spécifier le votre via le menu Fonds d'Ecran du panneau système. Vous pouvez choisir quel type de fond d'écran il s'agit.

Karaoke Mugen les affichera alors aléatoirement s'il y en a plusieurs.

### Musiques de pause

Par défaut il n'y a aucune musique de fond fournie avec Karaoke Mugen.

Vous pouvez spécifier des fichiers musicaux (mp3, m4a, ogg, flac, wav...) dans la page "Fond d'Ecran" de le panneau système. Ils seront lus durant les pauses entre chansons.

Si une musique porte le même nom qu'un fichier image (sans l'extension) celle-ci sera lue lorsque ce fichier image sera sélectionné au hasard. Sinon, une musique aléatoire sera choisie.

### Médias de liste de lecture

Si vous avez envie de pimenter un peu votre karaoké, vous pouvez passer des jingles, des sponsors toutes les XX chansons (via le paramètre [`Playlist.Medias`](#medias_1)), vous pouvez aussi passer des introductions, des outros et des _encore_. Toutes ces vidéos se trouvent par défaut respectivement dans les [dossiers](#dossiers) `jingles`, `sponsors`, `intros`, `outros`, `encores`.

La communauté Karaoke Mugen met à votre disposition quelques jingles vidéo, mais vous pouvez tout à fait utiliser les vôtres ! Si un fichier `.ass` porte le même nom que le fichier vidéo adjacent, celui-ci sera chargé automatiquement au moment où la vidéo passera.

Les médias sont téléchargeables [sur notre GitLab](https://gitlab.com/karaokemugen/medias). Placez-les dans les dossiers respectifs. Ils sont téléchargés automatiquement par Karaoke Mugen par défaut, ce comportement est [désactivable](#medias).
