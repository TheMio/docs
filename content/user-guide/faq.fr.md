+++
title = "FAQ"
weight = 7
+++

Foire aux questions !

Voici une liste de questions fréquemment posées concernant d'éventuels problèmes sur le logiciel.

## J'ai perdu mon mot de passe admin !

Pas de panique. Vous avez plusieurs options :

### Compte en ligne

* Utilisez le bouton "réinitialiser votre mot de passe". **Attention, cela ne fonctionnera que si vous avez entré une adresse mail dans votre profil**
    * Le cas échéant, vous êtes invité à [nous contacter](https://mugen.karaokes.moe/en/contact.html) pour qu'on puisse vous aider.

### Compte local

* Utilisez le bouton "réinitialiser votre mot de passe" sur la page de connexion et entrez le code de sécurité demandé (vous le trouverez dans le menu "Code de sécurité" de l'application tout en haut). Sur une version sans interface web (image Raspberry Pi par exemple), le code de sécurité se trouve dans les fichiers logs.

![password](/images/user/passwordlost.png)

## Ma liste de karaokés est vide ou incomplète

C'est peut-être normal, soit c'est votre premier lancement du logiciel, soit vous avez modifié incorrectement une option.

Si c'est la première fois que vous lancez ce logiciel, il devrait vous avoir téléchargé tous les karaokés. Cela peut prendre un peu de temps lors du premier (ou second) lancement si votre ordinateur ou votre disque dur est lent.

Dans la liste des dépôts du panneau système, assurez-vous que le dépôt `kara.moe` est activé et marqué comme en ligne.

Assurez-vous que la mise à jour n'est pas en cours avant de modifier quoi que ce soit.

Si besoin, supprimez le dépôt de la liste et recréez le en indiquant simplement `kara.moe` comme nom et en cochant "Activé" et "En ligne".

## Les .kara.json et .ass sont en place, mais j'ai toujours rien

Assurez-vous que :

- Votre base est correctement générée. Celle-ci se fait normalement au lancement lorsqu'une modification est détectée mais vous pouvez relancer une génération en allant dans le panneau SYSTEME depuis l'accueil, puis dans le menu **Base de données** et enfin en cliquant sur le bouton **Regenérer la base de données**.

![regen](/images/user/regeneration.png)

## J'ai correctement lancé le logiciel, mais la fenêtre vidéo est toute bleue/noire

Le problème est dû à votre carte graphique qui ne gère pas OpenGL. Il y a une option dans le fichier de configuration à modifier pour passer en Direct3D (windows uniquement)

Allez dans le panneau SYSTÈME, puis dans l'onglet **Système**, puis **Configuration avancée**, et éditez le paramètre `Player.mpvVideoOutput` en tapant `direct3d` dans la case.

## J'ai créé moi-même un karaoké, comment je fais pour le partager/l'intégrer à la base publique ?

Depuis la version 2.3 de Karaoke Mugen, il existe un formulaire pour ajouter vos karaokés.

[Suivez le guide](../../contrib-guide/create/) !

## Combien de place faut-il pour Karaoke Mugen ?

Le logiciel vous prendra dans les 500 Mo avec tous ses fichiers, mais tous les médias pèsent plusieurs centaines de giga-octets (vous pouvez voir la statistique actualisée sur la [page d'accueil de la base](https://kara.moe/base)).

## Peut-on récupérer une partie de la base seulement ?

Oui, le panneau système propose un gestionnaire de téléchargements qui vous permet de faire des selections précises sur ce que vous voulez télécharger.

Les médias seront également téléchargés automatiquement au fur et à mesure que vous en aurez besoin (lorsque vous les ajoutez à une liste de lecture courante).

![password](/images/user/download.png)