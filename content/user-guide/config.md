+++
title = "Configuration"
weight = 4
+++

Once Karaoke Mugen is working properly, you probably want to explore the possibilites or settings you can change to create the karaoke experience tailored to your taste.

Accessing to settings is done via the K menu in the upper-right corner in the [operator interface](../getting-started/#operator-interface), "Options" button. Alternatively, in the app menu bar, unfold "Options" then "Settings". We will be focusing on the Karaoke section, which is the most complete section, therefore the most complicated to understand.

## Quota and limits

With its default config, Karaoke Mugen allows everyone to add an unlimited number of songs to the playlist. There is no limit for users. You have many tools to put limits on users.

{{% notice note "Note" %}}
Those limits do not apply to administrators.
{{% /notice %}}

There are two types of quota:

- The **song number quota** allows restricting additions by the number of songs the users add.
- The **duration quota** allows restricting the cumulated duration of all songs. If you configure it to 20 minutes for example, each user will be able to add as many songs they want as long the sum of the duration is less than 20 minutes.

{{% notice warning "Warning" %}}
The two modes are exclusive, you cannot have both. 
{{% /notice %}}

The karaoke counter is shown as ![Karaokés restants : ∞](/images/user/quota_demo.png) in the top bar in the public interface.

### Freeing

The user recovers its quota, when one of the following occurs:

- the song goes on screen
- the song is freed by an operator (via the wrench menu <i class="fas fa-wrench"></i>)
  - the song is freed automatically when you accept or refuse the song
- if the freeing by vote is on, when other people likes the song

### Freeing by vote

You can configure your karaoke to have karaokes automatically freed if it receives likes from other users. You can configure a minimum percentage of connected users or an absolute minimum of users. Only one of these criteria is enough to free the karaoke automatically.

## Playlists

### Smart Insert and Balancing

If you are with friends and the [karaoke is managing itself](../playlists/#current-and-public-playlist), you can enable smart insert or playlist balancing. These two mechanisms are here to avoid monopolies, a person spamming their songs and taking all the space.

The smart insert will add below all other songs the songs from the users that haven't already added songs in the queue. The balancing will, when it can, pass one song from each user one-by-one (a song from user 1, then a song from user 2, then a song from 3, then a song again from user 1, then user 2, etc.).

### Playlist medias

By default, to augment your karaokes, Karaoke Mugen will show a sample of jingles and sponsors during the play. There is also intros and outros. Somes videos are included and you can [add yours](../advanced/#playlist-medias). For the jingles and the sponsors, you can configure the interval between 2 jingles or sponsors. For the others, you can configure a message to show automatically.

## Karaoke session

### Classic karaoke

The classic karaoke mode is to reproduce the experience that we can find in karaoke rooms. The player will pause iself at the end of each song and let the user who added the song to start it (an operator can also start the song). Perfect when you have a room with a microphone to give.

### Stream mode

The stream mode lets you place a pause (configurable delay) between each song to let someong that is doing all the karaokes to drink, breath, etc. (useful!).

#### Twitch Chat

If you're ready to sing all the songs that your public will suggest, you may want to activate the poll via Twitch Chat, the people will be able to vote in the chat one of the songs suggested by the public of your stream. Note that you couldn't control your karaoke now, so remember to [exclude karaokes](../playlists/#blacklists) that you don't want to see (we suggest you to exclude R18, epilepsy karaokes, there are misc tags in the database).

### Session end notification

If you configure a delay in minutes and you [configure the session](../getting-started/#sessions) with an end timestamp, you receive a notification to eventually prepare the end of your session if you have the leave the scene for example.

### Public vote

Want to let the public choose their songs? Public vote can be enabled in the admin interface and allows users to vote for songs they wish to be added next. The public interface will display a window asking users to vote for the song they'd like. Songs are selected from the [public playlist](../playlists/#public-playlist). If no song is the clear winner, a random song will be chosen amonog the ones with the most votes.

{{% notice tip "Tip" %}}
This is an ideal mode if you don't want to bother with choosing songs.
{{% /notice %}}

## Mystery songs

The operator can decide if what people add is visible or not in the playlist. Invisible songs count in the playlist but are displayed with a configurable name (by default ???)

It's great to create surprises for your guests, or simply to simply to make your playlist feel all mysterious.

The operator can make sure all the songs he/she adds are considered mystery songs.
