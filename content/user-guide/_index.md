+++
chapter = true
title = "User Guide"
weight = 1
+++

### User Guide

This section is about everything related to the Karaoke Mugen application and how it works.

## Table of Contents

{{% children depth="999" descriptions="true" %}}
