Please respect those rules when transcribing japanese script into latin characters when you want to add your karaoke [to Karaoke Mugen's base](http://kara.moe/base).

If it's for personal use or for another karaoke base, it is up to you.

- In a japanese karaoke, the non-japanese words have to be put in uppercase to show they're different. If we were keeping those in japanese it would be like: writing "ze wurld" for "The world" in a japanese karaoke featuring some english words. Examples:
	- zankoku na tenshi no **THESE**
	- ~~sumashu~~ **SMASH**
	- ~~imajineeshon~~ **IMAGINATION**
	- ~~doramatikku~~ **DRAMATIC**
  	- ~~purofesshonaru~~ **PROFESSIONAL**
- Particles have to be transcribed as **wa, wo, he** and not ha, o, e ("te **wo** tsunaide DANCIN' DANCIN'", "kimi no na ~~ha~~ **wa**").
- Long vowels as "ou" are written as such **and not** with a macron or a caron (mahou, ikouzo, hajimemashou, deshou).
- Do not put **caps** at the start of sentences (except in **non japanese** karaokes where it is allowed).
- Do not put **periods**. Commas, apostrophes, hyphens, tildes, elipsises, question and exclamation marks are allowed when they give hints on the lyrics' tones
- Do use caps for **proper names** (Japari Park, Kaguya-san, Ultraman).
