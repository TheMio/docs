* [Lyrical Nonsense](https://www.lyrical-nonsense.com/) :
Probablement l'un des meilleurs. Paroles en kanji + romajis voire plus. Ils sont très actifs quand il s'agit d'ajouter de nouvelles paroles et d'en modifier suite à la sortie du single (et donc du précieux booklet qui contient les vraies paroles). Pour savoir si l'une de leur pages est une transcription à l'oreille, scrollez en bas et vous aurez son *statut*, "official" ou "transliteration". Vous aurez aussi généralement la date de sortie officielle de la chanson.
* [VGMdb](https://vgmdb.net/db/main.php) : Une énorme base de données qui regroupe les singles / albums / OST sortis à ce jour. En vous rendant sur la page du single que vous cherchez, vous aurez parfois sur le côté droit dans le carré "Cover" des scans directs du booklet avec les paroles de votre chanson (voir plus bas).
* [Uta-Net](https://www.uta-net.com/) et [UtaMap](http://www.utamap.com/) : Les "Lyrical Nonsense" japonais (bases coopératives de paroles).
* [Anime Lyrics](https://www.animelyrics.com/)
* [Anime Song Lyrics](https://www.animesonglyrics.com/)
* [Asia Lyrics](https://www.azlyrics.com/a/asia.md)
