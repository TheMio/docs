Attention à bien respecter ces règles sur la transcription des idéogrammes en caractères latins lorsque vous souhaiter intégrer votre karaoké à [la base de Karaoke Mugen](http://kara.moe).

Si c'est pour votre utilisation personnelle ou à destination d'une autre base de karaokés, vous restez libre de vos choix.

- Dans un karaoké japonais, les mots non japonais doivent être mis en majuscules pour bien les différencier. Si on les laissait en japonais, ça équivaudrait à écrire "ze worlde" pour "The world" dans un karaoké en français mais contenant des mots anglais. Exemples :
	- zankoku na tenshi no **THESE**
	- ~~sumashu~~ **SMASH**
	- ~~imajineeshon~~ **IMAGINATION**
	- ~~doramatikku~~ **DRAMATIC**
	- ~~purofesshonaru~~ **PROFESSIONAL**
- Les particules doivent être retranscrite **wa, wo, he** et non ha, o, e ("te **wo** tsunaide DANCIN' DANCIN'", "kimi no na ~~ha~~ **wa**").
- Les voyelles longues comme "ou" sont notées ainsi **et pas** avec un macron ou un accent circonflexe (mahou, ikouzo, hajimemashou, deshou).
- Ne pas mettre de **majuscule** au début des phrases (sauf dans un karaoké **non japonais** où c'est permis).
- Ne pas mettre de **points**. Sont autorisés les virgules, apostrophes, tirets, tildes, points de suspension, d'interrogation et d'exclamation quand ils permettent de donner une indication sur l'intonation des paroles.
- Mettre une majuscule pour les **noms propres**, et pas tout en majuscule (Japari Park, Kaguya-san, Ultraman).
