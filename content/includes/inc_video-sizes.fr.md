Ce tableau est à titre indicatif : pour une résolution de vidéo donnée vous devriez utiliser le paramètre de Bitrate maximum indiqué ci-dessous pour ainsi obtenir une taille de vidéo acceptable.

La base de karaokés contenant plusieurs milliers de fichiers vidéo, il est important de faire des économies là où c'est possible. Hors de question d'avoir des fichiers de 300 Mo pour 1 minute 30 par exemple.

| **Résolution vidéo (axe Y)** | **Bitrate maximum** | **Poids fichier approximatif, pour 1 min 30** |
| -------------------------------- | ---------------------- | ------------------------------------------------------ |
| 1080p | 8000 kb/s | 80Mo |
| 720p | 4000 kb/s | 40Mo |
| 480p | 3000 kb/s | 30Mo |
| 360p | 2000 kb/s | 20Mo |